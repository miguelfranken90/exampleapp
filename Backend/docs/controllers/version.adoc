== Version Controller
=== /version
==== GET
.Request
include::{snippets}/VersionControllerTest/get-version-1/http-request.adoc[]

.Response
include::{snippets}/VersionControllerTest/get-version-1/response-body.adoc[]
