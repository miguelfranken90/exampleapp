package de.franken.appexamplebackend.dtos;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class DTO {

  protected String type = "UnknownType";

  public DTO(String type) {
    this.type = type;
  }

}
