package de.franken.appexamplebackend.dtos;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Getter
@Setter
public class EmployeeCreationDTO extends DTO {

  @Size(min=2, max=30)
  private String name;

  @NotBlank
  private String designation;

  @Max(1000)
  private double salary;

  public EmployeeCreationDTO() {
    super("EmployeeCreationDTO");
  }

  public EmployeeCreationDTO(String name, String designation, double salary) {
    this();
    this.name = name;
    this.designation = designation;
    this.salary = salary;
  }

}
