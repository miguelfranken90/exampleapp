package de.franken.appexamplebackend.dtos;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserDTO extends DTO {

  private long id;
  private String name;
  private String email;
  private boolean disabled;

  public UserDTO() {
    super("UserDTO");
  }

}
