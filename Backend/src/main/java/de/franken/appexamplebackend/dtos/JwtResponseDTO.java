package de.franken.appexamplebackend.dtos;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class JwtResponseDTO extends DTO {

  private String token;

  public JwtResponseDTO() {
    super("JwtResponseDTO");
  }

  public JwtResponseDTO(String token) {
    this();
    this.token = token;
  }

}
