package de.franken.appexamplebackend.dtos;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PostDTO extends DTO {

  private int userId;
  private int id;
  private String title;
  private String body;

  public PostDTO() {
    super("PostDTO");
  }

}
