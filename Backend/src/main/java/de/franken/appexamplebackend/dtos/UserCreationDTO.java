package de.franken.appexamplebackend.dtos;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserCreationDTO extends DTO {

  private String name;
  private String email;
  private String password;

  public UserCreationDTO() {
    super("UserCreationDTO");
  }

  public UserCreationDTO(String name, String email, String password) {
    this();
    this.name = name;
    this.email = email;
    this.password = password;
  }

}
