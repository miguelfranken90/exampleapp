package de.franken.appexamplebackend.dtos;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class JwtRequestDTO extends DTO {

  private String username;
  private String password;

  public JwtRequestDTO() {
    super("JwtRequestDTO");
  }

  public JwtRequestDTO(String username, String password) {
    this();
    this.username = username;
    this.password = password;
  }

}
