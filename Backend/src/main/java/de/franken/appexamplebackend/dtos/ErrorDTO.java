package de.franken.appexamplebackend.dtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import de.franken.appexamplebackend.errors.ExampleAppErrorCode;
import de.franken.appexamplebackend.errors.ExampleAppSubError;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import org.springframework.lang.Nullable;

import java.util.List;

@Getter
@Setter
public class ErrorDTO extends DTO {

  private int status;

  @NonNull
  private String message;
  private long timestamp;

  @NonNull
  private ExampleAppErrorCode errorCode;

  @Nullable
  @JsonInclude(JsonInclude.Include.NON_NULL)
  private List<ExampleAppSubError> subErrors;

  public ErrorDTO() {
    super("ErrorDTO");
  }

}
