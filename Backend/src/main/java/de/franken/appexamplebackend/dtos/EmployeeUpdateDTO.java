package de.franken.appexamplebackend.dtos;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Getter
@Setter
public class EmployeeUpdateDTO extends DTO {

  private long empId;

  @Size(min=2, max=30)
  private String name;

  @NotBlank
  private String designation;

  @Max(1000)
  private double salary;

  public EmployeeUpdateDTO() {
    super("EmployeeUpdateDTO");
  }

  public EmployeeUpdateDTO(long empId, String name, String designation, double salary) {
    this();
    this.empId = empId;
    this.name = name;
    this.designation = designation;
    this.salary = salary;
  }

}
