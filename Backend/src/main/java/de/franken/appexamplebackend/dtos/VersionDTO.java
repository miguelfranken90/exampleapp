package de.franken.appexamplebackend.dtos;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class VersionDTO extends DTO {

  @NotNull
  private String version;

  private String branch;
  private String commitId;

  public VersionDTO(String version) {
    super("VersionDTO");
    this.version = version;
  }

  public VersionDTO(String version, String branch, String commitId) {
    this(version);
    this.branch = branch;
    this.commitId = commitId;
  }

}
