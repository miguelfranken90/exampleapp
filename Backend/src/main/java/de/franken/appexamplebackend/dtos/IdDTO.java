package de.franken.appexamplebackend.dtos;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IdDTO extends DTO {

  private long id;

  private IdDTO() {
    super("IdDTO");
  }

  public IdDTO(long id) {
    this();
    this.id = id;
  }

}
