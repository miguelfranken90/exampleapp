package de.franken.appexamplebackend.entities;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class User {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private long id;

  private String name;

  private String email;

  private String password;

  private LocalDateTime registeredAt = LocalDateTime.now();

  private boolean disabled = false;

  private LocalDateTime lastLoginAt;

  @ManyToMany(cascade=CascadeType.MERGE, fetch = FetchType.EAGER)
  @JoinTable(
    name = "users_roles",
    joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"),
    inverseJoinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id")
  )
  private Collection<Role> roles;

  public User(String name, String email, String password) {
    this.name = name;
    this.email = email;
    this.password = password;
    this.disabled = false;
    this.roles = new ArrayList<>();
  }

  public User(String name, String email, String password, boolean disabled) {
    this(name, email, password);
    this.disabled = disabled;
  }

  public void addRole(Role role) {
    this.getRoles().add(role);
  }

}
