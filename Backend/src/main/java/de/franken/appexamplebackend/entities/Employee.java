package de.franken.appexamplebackend.entities;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Data
@NoArgsConstructor
public class Employee {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private long empId;

  private String name;
  private String designation;
  private double salary;

  public Employee(String name, String designation, double salary) {
    this.name = name;
    this.designation = designation;
    this.salary = salary;
  }

}
