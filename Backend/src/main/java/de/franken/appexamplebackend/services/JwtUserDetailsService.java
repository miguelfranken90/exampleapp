package de.franken.appexamplebackend.services;

import de.franken.appexamplebackend.dtos.UserCreationDTO;
import de.franken.appexamplebackend.entities.Privilege;
import de.franken.appexamplebackend.entities.Role;
import de.franken.appexamplebackend.errors.exceptions.UserAlreadyExistsException;
import de.franken.appexamplebackend.entities.User;
import de.franken.appexamplebackend.repositories.RoleRepository;
import de.franken.appexamplebackend.repositories.UserRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

@Service
@Log4j2
@Transactional
public class JwtUserDetailsService implements UserDetailsService {

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private RoleRepository roleRepository;

  private BCryptPasswordEncoder bcryptEncoder = new BCryptPasswordEncoder();

  /*
   * The Spring Security Authentication Manager calls this method for getting the user details from the database
   * when authenticating the user details provided by the user.
   */
  public UserDetails loadUserByUsername(String username) {
    log.debug("Trying to load user from DB with username " + username);

    Stream<User> users = StreamSupport.stream(userRepository.findAll().spliterator(), false);
    Optional<User> userOpt = users.filter(u -> username.equals(u.getName())).findAny();

    if (userOpt.isPresent()) {
      User user = userOpt.get();
      log.debug("Found user with username: " + username);
      return new org.springframework.security.core.userdetails.User(
              user.getName(), user.getPassword(), true,
          true, true, true, getAuthorities(user.getRoles())
      );
    } else {
      throw new UsernameNotFoundException("User not found with username: " + username);
    }
  }

  private Collection<? extends GrantedAuthority> getAuthorities(Collection<Role> rootRoles) {
    List<String> authorities = new ArrayList<>();
    List<Privilege> rootPrivileges = new ArrayList<>();

    if (rootRoles != null) {
      for (Role rootRole : rootRoles) {
        rootPrivileges.addAll(rootRole.getPrivileges());
        List<Role> roles = new ArrayList<>(getRoles(rootRole));
        roles.forEach(role -> authorities.add(role.getName()));
      }

      for (Privilege rootPrivilege : rootPrivileges) {
        List<Privilege> privileges = new ArrayList<>(getPrivileges(rootPrivilege));
        privileges.forEach(privilege -> authorities.add(privilege.getName()));
      }
    }

    return getGrantedAuthorities(authorities);
  }

  private Set<Privilege> getPrivileges(Privilege rootPrivilege) {
    Set<Privilege> privileges = new HashSet<>();
    privileges.addAll(this.getPrivileges(rootPrivilege, privileges));
    return privileges;
  }

  private Set<Privilege> getPrivileges(Privilege currentPrivilege, Set<Privilege> privileges) {
    privileges.add(currentPrivilege);
    for (Privilege privilege : currentPrivilege.getChildPrivileges()) {
      privileges.add(privilege);
      privileges.addAll(getPrivileges(privilege, privileges));
    }
    return privileges;
  }

  private Set<Role> getRoles(Role rootRole) {
    Set<Role> roles = new HashSet<>();
    roles.addAll(this.getRoles(rootRole, roles));
    return roles;
  }

  private Set<Role> getRoles(Role currentRole, Set<Role> roles) {
    roles.add(currentRole);
    for (Role role : currentRole.getChildRoles()) {
      roles.add(role);
      roles.addAll(getRoles(role, roles));
    }
    return roles;
  }

  // Privileges (and Roles) are mapped to GrantedAuthority entities
  private List<GrantedAuthority> getGrantedAuthorities(List<String> privileges) {
    List<GrantedAuthority> authorities = new ArrayList<>();
    for (String privilege : privileges) {
      authorities.add(new SimpleGrantedAuthority(privilege));
    }
    return authorities;
  }

  public void save(UserCreationDTO dto) {
    this.save(dto.getName(), dto.getEmail(), dto.getPassword());
  }

  public void save(User user) {
    this.save(user.getName(), user.getEmail(), user.getPassword());
  }

  public void save(String name, String email, String password) {
    if (userRepository.existsUserByName(name)) {
      throw new UserAlreadyExistsException(userRepository.findUserByName(name));
    } else {
      User newUser = User.builder()
          .name(name)
          .email(email)
          .password(bcryptEncoder.encode(password))
          .roles(Arrays.asList(roleRepository.findByName("ROLE_USER")))
          .build();
      userRepository.save(newUser);
    }
  }

}
