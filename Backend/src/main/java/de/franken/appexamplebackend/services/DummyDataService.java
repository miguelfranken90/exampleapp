package de.franken.appexamplebackend.services;

import de.franken.appexamplebackend.entities.Employee;
import de.franken.appexamplebackend.entities.Role;
import de.franken.appexamplebackend.entities.User;
import de.franken.appexamplebackend.repositories.EmployeeRepository;
import de.franken.appexamplebackend.repositories.RoleRepository;
import de.franken.appexamplebackend.repositories.UserRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

@Service
@Log4j2
public class DummyDataService {

  @Autowired
  private EmployeeRepository employeeRepository;

  @Autowired
  private RoleRepository roleRepository;

  @Autowired
  private UserRepository userRepository;

  private List<User> createUserList() {
    List<User> users = new ArrayList<>();

    Stream.of("admin2", "admin3").forEach(name -> {
      // https://www.javainuse.com/onlineBcrypt
      User user = new User(name, "a@b.com", "$2a$10$IS4YMvUyyk7gIKnq/riemOapDqv.TyfptkfVnKtp6Jt5m.5jSXYPe");
      Role adminRole = roleRepository.findByName("ROLE_ADMIN");
      user.setRoles(Arrays.asList(adminRole));
      users.add(user);
    });

    Stream.of("user").forEach(name -> {
      User user = new User(name, "a@b.com", "$2a$10$IS4YMvUyyk7gIKnq/riemOapDqv.TyfptkfVnKtp6Jt5m.5jSXYPe");
      Role userRole = roleRepository.findByName("ROLE_USER");
      user.setRoles(Arrays.asList(userRole));
      users.add(user);
    });

    return users;
  }

  private static List<Employee> createEmployeeList() {
    List<Employee> tempEmployees = new ArrayList<>();

    Employee emp1 = new Employee();
    emp1.setName("Wolfgang Schäfer");
    emp1.setDesignation("Professor");
    emp1.setSalary(100);

    Employee emp2 = new Employee();
    emp2.setName("Valentina Heinrich");
    emp2.setDesignation("Student");
    emp2.setSalary(110);

    Employee emp3 = new Employee();
    emp3.setName("Benedikt Martin");
    emp3.setDesignation("Student");
    emp3.setSalary(99);

    tempEmployees.add(emp1);
    tempEmployees.add(emp2);
    tempEmployees.add(emp3);

    return tempEmployees;
  }

  public void createDummyData() {
    log.info("Creating dummy data..");

    employeeRepository.saveAll(createEmployeeList());
    log.debug("Added employee dummy data");

    addUserDummyData();

    log.info("Created successfully dummy data");
  }

  private void addUserDummyData() {
    createUserList().forEach(user -> {
      if (userRepository.existsUserByName(user.getName())) {
        log.error("User with name '" + user.getName() + "' already exists. User cannot be created!");
      } else {
        userRepository.save(user);
      }
    });
    log.debug("Added user dummy data");
  }

}
