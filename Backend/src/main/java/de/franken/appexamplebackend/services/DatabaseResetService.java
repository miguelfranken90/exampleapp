package de.franken.appexamplebackend.services;

import de.franken.appexamplebackend.repositories.EmployeeRepository;
import de.franken.appexamplebackend.repositories.UserRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class DatabaseResetService {

  @Autowired
  private EmployeeRepository employeeRepository;

  @Autowired
  private UserRepository userRepository;

  public void reset() {
    log.info("Start resetting database..");
    employeeRepository.deleteAll();

    userRepository.findAll().forEach(user -> {
      if (!user.getName().equals("admin")) {
        userRepository.delete(user);
      }
    });
    log.info("Reset of database was successful");
  }

}
