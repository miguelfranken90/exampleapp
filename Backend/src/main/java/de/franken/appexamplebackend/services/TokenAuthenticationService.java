package de.franken.appexamplebackend.services;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class TokenAuthenticationService {

  static String JWT_SECRET = "javainuse";
  static final long EXPIRATION_TIME = 864_000_000; // 10 days
  static final String TOKEN_PREFIX = "Bearer";

  @Value("${jwt.secret}")
  public void setJwtSecret(String jwtSecret) {
    JWT_SECRET = jwtSecret;
  }

  public static String createToken(String username) {
    String jwt = Jwts.builder()
        .setSubject(username)
        .setExpiration(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
        .signWith(SignatureAlgorithm.HS512, JWT_SECRET)
        .compact();

    return TOKEN_PREFIX + " " + jwt;
  }

}
