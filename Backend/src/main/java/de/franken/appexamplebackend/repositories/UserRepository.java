package de.franken.appexamplebackend.repositories;

import de.franken.appexamplebackend.entities.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {
  boolean existsUserByName(String name);
  boolean existsUserById(long id);
  User findUserByName(String name);
}
