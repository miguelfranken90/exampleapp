package de.franken.appexamplebackend;

import de.franken.appexamplebackend.entities.Privilege;
import de.franken.appexamplebackend.entities.Role;
import de.franken.appexamplebackend.entities.User;
import de.franken.appexamplebackend.repositories.PrivilegeRepository;
import de.franken.appexamplebackend.repositories.RoleRepository;
import de.franken.appexamplebackend.repositories.UserRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

@Component
@Log4j2
public class InitialDataLoader implements ApplicationListener<ContextRefreshedEvent> {

  boolean alreadySetup = false;

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private RoleRepository roleRepository;

  @Autowired
  private PrivilegeRepository privilegeRepository;

  @Override
  @Transactional
  public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
    log.info("Initial Data Loader");

    if (alreadySetup)
      return;

    Privilege employeePrivilege = createPrivilegeIfNotFound("EMPLOYEE");

    Privilege readPrivilege = createPrivilegeIfNotFound(employeePrivilege, "READ_EMPLOYEE");
    Privilege createPrivilege = createPrivilegeIfNotFound(employeePrivilege, "CREATE_EMPLOYEE");
    Privilege deletePrivilege = createPrivilegeIfNotFound(employeePrivilege, "DELETE_EMPLOYEE");
    Privilege updatePrivilege = createPrivilegeIfNotFound(employeePrivilege, "UPDATE_EMPLOYEE");

    List<Privilege> adminPrivileges = Arrays.asList(employeePrivilege);

    Role adminRole = createRoleIfNotFound("ROLE_ADMIN", adminPrivileges);
    Role userRole = createRoleIfNotFound(adminRole, "ROLE_USER", Arrays.asList(readPrivilege));

    User user = new User();
    user.setName("admin");
    user.setPassword("$2a$10$IS4YMvUyyk7gIKnq/riemOapDqv.TyfptkfVnKtp6Jt5m.5jSXYPe"); // "passwort"
    user.setEmail("test@test.com");
    user.setRoles(Arrays.asList(adminRole));
    user.setDisabled(false);
    if (!userRepository.existsUserByName(user.getName())) {
      userRepository.save(user);
    }

    alreadySetup = true;
  }

  @Transactional
  Privilege createPrivilegeIfNotFound(String name) {
    Privilege privilege = privilegeRepository.findByName(name);
    if (privilege == null) {
      privilege = Privilege.builder().name(name).childPrivileges(new ArrayList<>()).build();
      privilegeRepository.save(privilege);
    }
    return privilege;
  }

  @Transactional
  Privilege createPrivilegeIfNotFound(Privilege parentPrivilege, String name) {
    Privilege privilege = this.createPrivilegeIfNotFound(name);

    if (!parentPrivilege.getChildPrivileges().contains(privilege)) {
      parentPrivilege.getChildPrivileges().add(privilege);
    }
    return privilege;
  }

  @Transactional
  Role createRoleIfNotFound(String name, Collection<Privilege> privileges) {
    Role role = roleRepository.findByName(name);
    if (role == null) {
      role = Role.builder().name(name).privileges(privileges).childRoles(new ArrayList<>()).build();
      roleRepository.save(role);
    }
    return role;
  }

  @Transactional
  Role createRoleIfNotFound(Role parentRole, String name, Collection<Privilege> privileges) {
    Role role = this.createRoleIfNotFound(name, privileges);

    if (!parentRole.getChildRoles().contains(role)) {
      parentRole.getChildRoles().add(role);
    }
    return role;
  }

}
