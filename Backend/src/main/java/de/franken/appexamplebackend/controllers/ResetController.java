package de.franken.appexamplebackend.controllers;

import de.franken.appexamplebackend.dtos.OkDTO;
import de.franken.appexamplebackend.services.DatabaseResetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping({ "/resetDB" })
public class ResetController {

  @Autowired
  private DatabaseResetService databaseResetService;

  @GetMapping(produces = "application/json")
  public OkDTO reset() {
    databaseResetService.reset();
    return new OkDTO();
  }

}
