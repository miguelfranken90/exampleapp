package de.franken.appexamplebackend.controllers;

import de.franken.appexamplebackend.dtos.PostDTO;
import de.franken.appexamplebackend.services.RestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/posts")
public class PostController {

  @Autowired
  private RestService restService;

  @GetMapping
  public PostDTO[] getPosts() {
    return restService.getPosts();
  }

}
