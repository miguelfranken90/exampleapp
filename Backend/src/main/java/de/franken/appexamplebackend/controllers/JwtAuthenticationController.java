package de.franken.appexamplebackend.controllers;

import de.franken.appexamplebackend.config.JwtTokenUtil;
import de.franken.appexamplebackend.dtos.*;
import de.franken.appexamplebackend.errors.exceptions.UnauthorizedException;
import de.franken.appexamplebackend.repositories.UserRepository;
import de.franken.appexamplebackend.services.JwtUserDetailsService;
import io.jsonwebtoken.ExpiredJwtException;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

/* see https://www.javainuse.com/spring/boot-jwt for more info concerning JWT authentication */
@RestController
@Log4j2
public class JwtAuthenticationController {

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private AuthenticationManager authenticationManager;

  @Autowired
  private JwtTokenUtil jwtTokenUtil;

  @Autowired
  private JwtUserDetailsService userDetailsService;

  @PostMapping(value = "/register")
  public IdDTO saveUser(@RequestBody UserCreationDTO dto) {
    userDetailsService.save(dto);
    return new IdDTO(userRepository.findUserByName(dto.getName()).getId());
  }

  /*
   * Using Spring Authentication Manager we authenticate the username and password.
   * If the credentials are valid, a JWT token is created using the JWTTokenUtil and provided to the client.
   */
  @PostMapping(value = "/authenticate")
  public JwtResponseDTO createAuthenticationToken(@RequestBody JwtRequestDTO authenticationRequest) {
    // authenticates use or throws exception if authentication failed
    authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(authenticationRequest.getUsername(), authenticationRequest.getPassword()));

    final UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getUsername());
    final String token = jwtTokenUtil.generateToken(userDetails);

    return new JwtResponseDTO(token);
  }

  @PostMapping(value = "/validate")
  public OkDTO validateToken(@RequestBody String token) throws Exception {
    try {
      log.info("Validating token..");
      String username = jwtTokenUtil.getUsernameFromToken(token);
      UserDetails user = this.userDetailsService.loadUserByUsername(username);
      if (jwtTokenUtil.validateToken(token, user)) {
        return new OkDTO();
      } else {
        throw new Exception();
      }
    } catch (Exception e) {
      throw new UnauthorizedException();
    }
  }

}
