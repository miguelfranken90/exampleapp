package de.franken.appexamplebackend.controllers;

import de.franken.appexamplebackend.dtos.IdDTO;
import de.franken.appexamplebackend.dtos.OkDTO;
import de.franken.appexamplebackend.dtos.UserCreationDTO;
import de.franken.appexamplebackend.dtos.UserDTO;
import de.franken.appexamplebackend.errors.exceptions.UserAlreadyExistsException;
import de.franken.appexamplebackend.errors.exceptions.UserNotFoundException;
import de.franken.appexamplebackend.repositories.UserRepository;
import de.franken.appexamplebackend.entities.User;
import lombok.extern.log4j.Log4j2;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/users")
@Log4j2
public class UserController {

  private final UserRepository userRepository;

  /*
   * Constructor-based dependency injection
   * See https://blog.marcnuri.com/field-injection-is-not-recommended/
   */
  @Autowired
  public UserController(UserRepository userRepository) {
    this.userRepository = userRepository;
  }

  @GetMapping()
  public List<UserDTO> getUsers() {
    /* see https://www.mkyong.com/logging/log4j-hello-world-example/ for more info concerning logging */
    log.debug("Send all users to frontend");

    ModelMapper modelMapper = new ModelMapper();
    List<UserDTO> dtos = new ArrayList<>();
    userRepository.findAll().forEach(user -> {
      UserDTO dto = modelMapper.map(user, UserDTO.class);
      dtos.add(dto);
    });

    return dtos;
  }

  @PostMapping()
  public IdDTO addUser(@RequestBody UserCreationDTO dto) {
    log.debug("Saving user from dto {}..", dto);

    if (userRepository.existsUserByName(dto.getName())) {
      throw new UserAlreadyExistsException(userRepository.findUserByName(dto.getName()));
    } else {
      User user = new User(dto.getName(), dto.getEmail(), dto.getPassword());
      userRepository.save(user);
      return new IdDTO(user.getId());
    }
  }

  @PreAuthorize("hasRole('ADMIN')")
  @DeleteMapping(path = {"/{id}"})
  public OkDTO delete(@PathVariable("id") long id) {
    Optional<User> user = this.userRepository.findById(id);
    if (user.isPresent()) {
      this.userRepository.deleteById(id);
      log.debug("Deleted user " + user.toString() + "from repository");
      return new OkDTO();
    } else {
      throw new UserNotFoundException("User to delete not found");
    }
  }
}
