package de.franken.appexamplebackend.controllers;

import de.franken.appexamplebackend.dtos.EmployeeCreationDTO;
import de.franken.appexamplebackend.dtos.EmployeeUpdateDTO;
import de.franken.appexamplebackend.dtos.OkDTO;
import de.franken.appexamplebackend.entities.Employee;
import de.franken.appexamplebackend.repositories.EmployeeRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping({ "/employees" })
@Log4j2
public class EmployeeController {

  private EmployeeRepository employeeRepository;

  @Autowired
  public EmployeeController(EmployeeRepository employeeRepository) {
    this.employeeRepository = employeeRepository;
  }

  @GetMapping(produces = "application/json")
  public List<Employee> firstPage() {
    log.debug("Send all employees to frontend");
    return (List<Employee>) this.employeeRepository.findAll();
  }

  @PreAuthorize("hasRole('ADMIN')")
  @DeleteMapping(path = { "/{id}" })
  public Employee delete(@PathVariable("id") long id) {
    Optional<Employee> emp = this.employeeRepository.findById(id);
    if (emp.isPresent()) {
      this.employeeRepository.deleteById(id);
      log.debug("Deleted employee " + emp.toString() + " from repository");
      return emp.get();
    } else {
      throw new EntityNotFoundException("Employee to delete not found");
    }
  }

  @PostMapping
  public Employee create(@Valid @RequestBody EmployeeCreationDTO dto) {
    Employee employee = new Employee(dto.getName(), dto.getDesignation(), dto.getSalary());
    this.employeeRepository.save(employee);
    log.debug("Saved received employee into repository");
    return employee;
  }

  @PostMapping(path = { "/update" })
  public OkDTO update(@Valid @RequestBody EmployeeUpdateDTO dto) {
    Optional<Employee> employeeOpt = this.employeeRepository.findById(dto.getEmpId());
    if (employeeOpt.isPresent()) {
      Employee employee = employeeOpt.get();
      employee.setName(dto.getName());
      employee.setDesignation(dto.getDesignation());
      employee.setSalary(dto.getSalary());
      this.employeeRepository.save(employee);
    } else {
      throw new EntityNotFoundException("Cannot find employee with id " + dto.getEmpId());
    }
    return new OkDTO();
  }

}
