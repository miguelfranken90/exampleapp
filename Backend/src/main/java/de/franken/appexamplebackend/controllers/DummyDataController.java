package de.franken.appexamplebackend.controllers;

import de.franken.appexamplebackend.dtos.OkDTO;
import de.franken.appexamplebackend.services.DatabaseResetService;
import de.franken.appexamplebackend.services.DummyDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping({ "/createDummyData" })
public class DummyDataController {

  @Autowired
  private DummyDataService dummyDataService;

  @Autowired
  private DatabaseResetService databaseResetService;

  @GetMapping(produces = "application/json")
  public OkDTO createDummyData() {
    databaseResetService.reset();
    dummyDataService.createDummyData();
    return new OkDTO();
  }

}
