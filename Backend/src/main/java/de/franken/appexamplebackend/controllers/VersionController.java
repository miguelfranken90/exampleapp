package de.franken.appexamplebackend.controllers;

import de.franken.appexamplebackend.dtos.VersionDTO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/version")
public class VersionController {

  @Value("${build.version}")
  private String version;

  @Value("${git.branch}")
  private String branch;

  @Value("${git.commit.id}")
  private String commitId;

  @GetMapping
  public VersionDTO getVersion() {
    return new VersionDTO(version, branch, commitId);
  }

}
