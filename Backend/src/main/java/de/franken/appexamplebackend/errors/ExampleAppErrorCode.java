package de.franken.appexamplebackend.errors;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum ExampleAppErrorCode {
  @JsonProperty("ERRx0000")
  UNAUTHORIZED,

  @JsonProperty("ERRx0001")
  USER_NOT_FOUND,

  @JsonProperty("ERRx0002")
  USER_ALREADY_EXISTS,

  @JsonProperty("ERRx0003")
  USER_IS_DISABLED,

  @JsonProperty("ERRx0004")
  INVALID_AUTHENTICATION_CREDENTIALS,

  @JsonProperty("ERRx0005")
  ENTITY_NOT_FOUND,

  @JsonProperty("ERRx0006")
  UNEXPECTED,

  @JsonProperty("ERRx0007")
  REST_CLIENT_ERROR,

  @JsonProperty("ERRx0008")
  VALIDATION_ERROR
}
