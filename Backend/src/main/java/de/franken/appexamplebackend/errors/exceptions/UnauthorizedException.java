package de.franken.appexamplebackend.errors.exceptions;

public class UnauthorizedException extends RuntimeException {

  public UnauthorizedException() {
    super("User is not authorized");
  }

}
