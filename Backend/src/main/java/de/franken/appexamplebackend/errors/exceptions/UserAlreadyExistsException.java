package de.franken.appexamplebackend.errors.exceptions;

import de.franken.appexamplebackend.entities.User;
import lombok.Getter;

@Getter
public class UserAlreadyExistsException extends RuntimeException {

  private final User user;

  public UserAlreadyExistsException(User user) {
    super("User with name '" + user.getName() + "' cannot be added to database as a user with this name already exists");
    this.user = user;
  }
}
