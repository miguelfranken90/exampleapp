package de.franken.appexamplebackend.errors;

import de.franken.appexamplebackend.dtos.ErrorDTO;
import de.franken.appexamplebackend.errors.exceptions.UnauthorizedException;
import de.franken.appexamplebackend.errors.exceptions.UserAlreadyExistsException;
import de.franken.appexamplebackend.errors.exceptions.UserNotFoundException;
import lombok.extern.log4j.Log4j2;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.security.authentication.DisabledException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.persistence.EntityNotFoundException;
import java.util.HashMap;
import java.util.Map;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
@Log4j2
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

  private ResponseEntity<Object> buildResponseEntity(ExampleAppError error) {
    ErrorDTO dto = new ErrorDTO();

    dto.setMessage(error.description);
    dto.setStatus(error.httpStatus.value());
    dto.setTimestamp(System.currentTimeMillis());
    dto.setErrorCode(error.errorCode);

    if (error.getSubErrors() != null) {
      dto.setSubErrors(error.getSubErrors());
    }

    return new ResponseEntity<>(dto, error.httpStatus);
  }

  @Override
  protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
    log.debug("RestClientException was thrown", ex);

    Map<String, String> errors = new HashMap<>();
    ex.getBindingResult().getAllErrors().forEach((error) -> {
      String fieldName = ((FieldError) error).getField();
      String errorMessage = error.getDefaultMessage();
      errors.put(fieldName, errorMessage);
    });

    return buildResponseEntity(ExampleAppErrorFactory.validationError(errors));
  }

  @ExceptionHandler(UserNotFoundException.class)
  protected ResponseEntity<Object> handleUserNotFound(UserNotFoundException ex) {
    log.debug("UserNotFoundException was thrown", ex);
    return buildResponseEntity(ExampleAppErrorFactory.userNotFound());
  }

  @ExceptionHandler(UnauthorizedException.class)
  protected ResponseEntity<Object> handleUnauthorizedRequest(UnauthorizedException ex) {
    log.debug("UnauthorizedException was thrown", ex);
    return buildResponseEntity(ExampleAppErrorFactory.unauthorized());
  }

  @ExceptionHandler(UserAlreadyExistsException.class)
  protected ResponseEntity<Object> handleDuplicateUserCreationAttempt(UserAlreadyExistsException ex) {
    log.debug("UserAlreadyExistsException was thrown", ex);
    return buildResponseEntity(ExampleAppErrorFactory.userAlreadyExists(ex.getUser()));
  }

  @ExceptionHandler(DisabledException.class)
  protected ResponseEntity<Object> handleDisabledUser(DisabledException ex) {
    log.debug("DisabledException was thrown", ex);
    return buildResponseEntity(ExampleAppErrorFactory.userDisabled());
  }

  @ExceptionHandler(BadCredentialsException.class)
  protected ResponseEntity<Object> handleDisabledUser(BadCredentialsException ex) {
    log.debug("BadCredentialsException was thrown", ex);
    return buildResponseEntity(ExampleAppErrorFactory.invalidAuthenticationCredentials());
  }

  @ExceptionHandler(EntityNotFoundException.class)
  protected ResponseEntity<Object> handleNotFoundEntity(EntityNotFoundException ex) {
    log.debug("EntityNotFoundException was thrown", ex);
    return buildResponseEntity(ExampleAppErrorFactory.entityNotFound(ex.getMessage()));
  }

  @ExceptionHandler(RestClientException.class)
  protected ResponseEntity<Object> handleRestClientException(RestClientException ex) {
    log.debug("RestClientException was thrown", ex);
    return buildResponseEntity(ExampleAppErrorFactory.restClientError(ex.getMessage()));
  }

  @ExceptionHandler(Exception.class)
  protected ResponseEntity<Object> handleUnexpectedError(Exception ex) {
    log.debug("Exception was thrown", ex);
    return buildResponseEntity(ExampleAppErrorFactory.unexpectedError(ex.getMessage()));
  }

}
