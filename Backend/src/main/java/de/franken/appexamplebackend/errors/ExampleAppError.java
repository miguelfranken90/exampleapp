package de.franken.appexamplebackend.errors;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NonNull;
import org.springframework.http.HttpStatus;
import org.springframework.lang.Nullable;

import java.util.List;

@Data
@AllArgsConstructor
public class ExampleAppError {

  @NonNull
  HttpStatus httpStatus;

  @NonNull
  ExampleAppErrorCode errorCode;

  @NonNull
  String description;

  @Nullable
  List<ExampleAppSubError> subErrors;

  public ExampleAppError(@NonNull HttpStatus httpStatus, @NonNull ExampleAppErrorCode errorCode, @NonNull String description) {
    this.httpStatus = httpStatus;
    this.errorCode = errorCode;
    this.description = description;
  }

}
