package de.franken.appexamplebackend.errors;

import de.franken.appexamplebackend.entities.User;
import org.springframework.http.HttpStatus;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ExampleAppErrorFactory {

  private ExampleAppErrorFactory() {
    throw new IllegalStateException("ExampleAppErrorFactory is just a utility class");
  }

  public static ExampleAppError unauthorized() {
    return new ExampleAppError(HttpStatus.UNAUTHORIZED, ExampleAppErrorCode.UNAUTHORIZED, "User is not authorized");
  }

  public static ExampleAppError userNotFound() {
    return new ExampleAppError(HttpStatus.NOT_FOUND, ExampleAppErrorCode.USER_NOT_FOUND, "User could not be found");
  }

  public static ExampleAppError userAlreadyExists(User user) {
    return new ExampleAppError(HttpStatus.CONFLICT, ExampleAppErrorCode.USER_ALREADY_EXISTS, "User with name '" + user.getName() + "' already exists");
  }

  public static ExampleAppError userDisabled() {
    return new ExampleAppError(HttpStatus.UNAUTHORIZED, ExampleAppErrorCode.USER_IS_DISABLED, "User is disabled");
  }

  public static ExampleAppError invalidAuthenticationCredentials() {
    return new ExampleAppError(HttpStatus.UNAUTHORIZED, ExampleAppErrorCode.INVALID_AUTHENTICATION_CREDENTIALS, "Cannot authenticate user as given authentication credentials are invalid");
  }

  public static ExampleAppError entityNotFound(String description) {
    return new ExampleAppError(HttpStatus.NOT_FOUND, ExampleAppErrorCode.ENTITY_NOT_FOUND, "Entity not found: " + description);
  }

  public static ExampleAppError unexpectedError(String description) {
    return new ExampleAppError(HttpStatus.INTERNAL_SERVER_ERROR, ExampleAppErrorCode.UNEXPECTED, "Unexpected error occurred: " + description);
  }

  public static ExampleAppError restClientError(String description) {
    return new ExampleAppError(HttpStatus.INTERNAL_SERVER_ERROR, ExampleAppErrorCode.REST_CLIENT_ERROR, "RestClient error occurred: " + description);
  }

  public static ExampleAppError validationError(Map<String, String> errors) {
    List<ExampleAppSubError> subErrors = new ArrayList<>();
    errors.keySet().forEach(fieldName -> {
      StringBuilder errorMessageBuilder = new StringBuilder("");
      errorMessageBuilder.append(fieldName).append(": ").append(errors.get(fieldName));
      ExampleAppSubError subError = new ExampleAppSubError(errorMessageBuilder.toString());
      subErrors.add(subError);
    });

    return new ExampleAppError(HttpStatus.BAD_REQUEST, ExampleAppErrorCode.VALIDATION_ERROR, "Validation error occurred", subErrors);
  }

}
