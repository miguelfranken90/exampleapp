package de.franken.appexamplebackend.errors;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ExampleAppSubError {
  String description;
}
