package de.franken.appexamplebackend.config;

import de.franken.appexamplebackend.entities.User;
import de.franken.appexamplebackend.services.JwtUserDetailsService;
import de.franken.appexamplebackend.services.TokenAuthenticationService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockFilterChain;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import javax.servlet.ServletException;

import java.io.IOException;
import java.util.ArrayList;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.notNull;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class JwtRequestFilterTest {

  private static final String testUri = "/users";

  @Mock
  private JwtTokenUtil jwtTokenUtil;

  @Mock
  private JwtUserDetailsService jwtUserDetailsService;

  @InjectMocks
  private JwtRequestFilter tokenAuthenticationFilter = new JwtRequestFilter();

  @AfterEach
  public void afterEach() {
    SecurityContextHolder.getContext().setAuthentication(null);
  }

  private void test(boolean withValidToken) throws ServletException, IOException {
    // compute dummy token
    User user = new User("myDummyUsername", "a@b.com", "myPassword");
    String token = TokenAuthenticationService.createToken(user.getName());

    // mocking JwtTokenUtil and JwtUserDetailsService components
    when(jwtTokenUtil.getUsernameFromToken(notNull())).thenReturn("myDummyUsername");
    when(jwtTokenUtil.validateToken(notNull(), notNull())).thenReturn(withValidToken);
    UserDetails dummyUserDetails = new org.springframework.security.core.userdetails.User(user.getName(), user.getPassword(), new ArrayList<>());
    when(jwtUserDetailsService.loadUserByUsername(notNull())).thenReturn(dummyUserDetails);

    // mock HTTP request and response
    MockHttpServletRequest request = new MockHttpServletRequest();
    request.addHeader("Authorization", token);
    request.setRequestURI(testUri);
    MockHttpServletResponse response = new MockHttpServletResponse();

    // apply filter
    MockFilterChain filterChain = new MockFilterChain();
    tokenAuthenticationFilter.doFilterInternal(request, response, filterChain);

    // test filter
    if (withValidToken) {
      assertTrue(SecurityContextHolder.getContext().getAuthentication().isAuthenticated());
    } else {
      assertThrows(Exception.class, () -> SecurityContextHolder.getContext().getAuthentication().isAuthenticated());
    }
    assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
  }

  @Test
  public void testWithValidToken() throws ServletException, IOException {
    this.test(true);
  }

  @Test
  public void testWithInvalidToken() throws ServletException, IOException {
    this.test(false);
  }

}
