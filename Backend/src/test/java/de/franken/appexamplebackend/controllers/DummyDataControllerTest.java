package de.franken.appexamplebackend.controllers;

import de.franken.appexamplebackend.AbstractTest;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@SpringBootTest
class DummyDataControllerTest extends AbstractTest {

  @Test
  @DisplayName("Should send back OkDTO")
  void createDummyData() throws Exception {
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/createDummyData");
    mockMvc.perform(requestBuilder).andDo(print()).andExpect(OkDTO).andExpect(OkStatusCode).andDo(createDocumentation(false));
  }

}
