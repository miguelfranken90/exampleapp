package de.franken.appexamplebackend.controllers;

import de.franken.appexamplebackend.AbstractTest;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@SpringBootTest
class PostControllerTest extends AbstractTest {

  @Test
  void getPosts() throws Exception {
    String token = createDummyToken();
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/posts").header("Authorization", token);;
    mockMvc.perform(requestBuilder).andDo(print()).andExpect(PostDTO).andExpect(OkStatusCode);
  }

}
