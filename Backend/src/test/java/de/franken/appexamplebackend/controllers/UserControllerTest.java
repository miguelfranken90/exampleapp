package de.franken.appexamplebackend.controllers;

import de.franken.appexamplebackend.AbstractTest;
import de.franken.appexamplebackend.dtos.UserCreationDTO;
import de.franken.appexamplebackend.repositories.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import javax.transaction.Transactional;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
public class UserControllerTest extends AbstractTest {

  @Autowired
  private UserRepository userRepository;

  @Test
  @WithMockUser // https://stackoverflow.com/a/47575089
  public void testGetRequest() throws Exception {
    String token = createDummyToken();

    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders
        .get("/users")
        .header("Authorization", token);

    mockMvc.perform(requestBuilder)
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().string(containsString("a@b.com")))
        .andDo(createDocumentation(true));
  }

  @Test
  @WithMockUser
  public void testPostRequest() throws Exception {
    String token = createDummyToken();

    long numberOfUsers = userRepository.count();
    long expectedNumberOfUsers = numberOfUsers + 1;

    UserCreationDTO newUser = new UserCreationDTO("dummyUserForPostRequest", "a@b.com", "myPassword");
    Assertions.assertFalse(userRepository.existsUserByName(newUser.getName()));

    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders
        .post("/users")
        .header("Authorization", token)
        .content(asJsonString(newUser))
        .contentType(MediaType.APPLICATION_JSON);

    mockMvc.perform(requestBuilder).andExpect(OkStatusCode).andDo(print()).andExpect(IdDTO).andDo(createDocumentation(true));
    Assertions.assertEquals(expectedNumberOfUsers, userRepository.count());
    Assertions.assertTrue(userRepository.existsUserByName(newUser.getName()));

    // It should not be possible to save two users with the same name
    mockMvc.perform(requestBuilder).andExpect(ConflictStatusCode).andExpect(ErrorDTO).andDo(print()).andDo(createDocumentation(true));
    Assertions.assertEquals(expectedNumberOfUsers, userRepository.count());
  }

  @Test
  @DisplayName("Admin can delete users")
  public void testDeleteRequest1() throws Exception {
    String token = createDummyToken("ROLE_ADMIN");

    long numberOfUsers = userRepository.count();
    Assertions.assertTrue(numberOfUsers > 0);
    long adminId = userRepository.findUserByName("admin").getId();

    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders
        .delete("/users/{id}", adminId)
        .header("Authorization", token);

    mockMvc.perform(requestBuilder).andExpect(OkStatusCode).andExpect(OkDTO).andDo(createDocumentation(true));

    long newNumberOfUsers = userRepository.count();
    Assertions.assertEquals(numberOfUsers - 1, newNumberOfUsers);
  }

  @Test
  @DisplayName("Only admins can delete users")
  public void testDeleteRequest2() throws Exception {
    String token = createDummyToken("ROLE_USER");

    long numberOfUsers = userRepository.count();
    Assertions.assertTrue(numberOfUsers > 0);
    long adminId = userRepository.findUserByName("admin2").getId();

    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders
        .delete("/users/{id}", adminId)
        .header("Authorization", token);

    mockMvc.perform(requestBuilder).andExpect(ErrorDTO).andDo(createDocumentation(true));

    long newNumberOfUsers = userRepository.count();
    Assertions.assertEquals(numberOfUsers, newNumberOfUsers);
  }

  @Test
  @WithMockUser
  public void testDeleteRequestUserNotFoundException() throws Exception {
    String token = createDummyToken("ROLE_ADMIN");

    long numberOfUsers = userRepository.count();

    long nonexistingUserId = 5000L;
    Assertions.assertFalse(userRepository.existsUserById(nonexistingUserId));

    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders
        .delete("/users/{id}", nonexistingUserId)
        .header("Authorization", token);

    mockMvc.perform(requestBuilder).andExpect(NotFoundStatusCode).andExpect(ErrorDTO).andDo(createDocumentation(true));

    Assertions.assertEquals(numberOfUsers, userRepository.count());
  }

}
