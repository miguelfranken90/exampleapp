package de.franken.appexamplebackend.controllers;

import de.franken.appexamplebackend.AbstractTest;
import de.franken.appexamplebackend.dtos.JwtRequestDTO;
import de.franken.appexamplebackend.dtos.UserCreationDTO;
import de.franken.appexamplebackend.entities.User;
import de.franken.appexamplebackend.repositories.UserRepository;
import de.franken.appexamplebackend.services.JwtUserDetailsService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

@SpringBootTest
class JwtAuthenticationControllerTest extends AbstractTest {

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private JwtUserDetailsService jwtUserDetailsService;

  @Test
  @DisplayName("Should be able to register a previously nonexistent user")
  public void testRegister() throws Exception {
    UserCreationDTO newUser = new UserCreationDTO("myNewUsername", "a@b.com", "myPassword");
    Assertions.assertFalse(userRepository.existsUserByName(newUser.getName()));

    long numberOfUsers = userRepository.count();
    long expectedNumberOfUsers = numberOfUsers + 1;

    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders
        .post("/register")
        .content(asJsonString(newUser))
        .contentType("application/json");

    mockMvc.perform(requestBuilder).andDo(print()).andExpect(OkStatusCode).andExpect(IdDTO).andDo(createDocumentation(false));

    Assertions.assertEquals(expectedNumberOfUsers, userRepository.count());
    Assertions.assertTrue(userRepository.existsUserByName(newUser.getName()));
  }

  @Test
  @DisplayName("Should not be able to register a user that already exists")
  public void testRegister2() throws Exception {
    User user = new User("alreadyExistingUsername", "a@b.com", "myPassword");
    ModelMapper modelMapper = new ModelMapper();
    UserCreationDTO dto = modelMapper.map(user, UserCreationDTO.class);
    userRepository.save(user);
    Assertions.assertTrue(userRepository.existsUserByName(dto.getName()));

    long numberOfUsers = userRepository.count();

    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders
        .post("/register")
        .content(asJsonString(dto))
        .contentType("application/json");

    mockMvc.perform(requestBuilder).andDo(print()).andExpect(ConflictStatusCode).andExpect(ErrorDTO).andDo(createDocumentation(false));

    Assertions.assertEquals(numberOfUsers, userRepository.count());
  }

  @Test
  @DisplayName("Should be able to authenticate user with correct credentials")
  public void authenticateTest1() throws Exception {
    User user = new User("myCorrectUsername", "a@b.com", "myCorrectPassword");
    Assertions.assertFalse(userRepository.existsUserByName(user.getName()));
    jwtUserDetailsService.save(user);
    Assertions.assertTrue(userRepository.existsUserByName(user.getName()));

    JwtRequestDTO jwtRequestDTO = new JwtRequestDTO(user.getName(), user.getPassword());

    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders
        .post("/authenticate")
        .content(asJsonString(jwtRequestDTO))
        .contentType("application/json");

    mockMvc.perform(requestBuilder)
        .andDo(print())
        .andExpect(OkStatusCode)
        .andExpect(content().string(containsString("token")))
        .andDo(createDocumentation(false));
  }

  @Test
  @DisplayName("Should not authenticate disabled users")
  @Disabled
  public void authenticateTest2() {
    // TODO
  }

  @Test
  @DisplayName("Should not authenticate with invalid credentials")
  public void authenticateTest3() throws Exception {
    User user = new User("myUsername", "a@b.com", "myCorrectPassword");
    Assertions.assertFalse(userRepository.existsUserByName(user.getName()));
    jwtUserDetailsService.save(user);
    Assertions.assertTrue(userRepository.existsUserByName(user.getName()));

    String incorrectPassword = "myIncorrectPassword";
    Assertions.assertNotEquals(user.getPassword(), incorrectPassword);

    JwtRequestDTO jwtRequestDTO = new JwtRequestDTO(user.getName(), incorrectPassword);

    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders
        .post("/authenticate")
        .content(asJsonString(jwtRequestDTO))
        .contentType("application/json");

    mockMvc.perform(requestBuilder)
        .andDo(print())
        .andExpect(UnauthorizedStatusCode)
        .andExpect(ErrorDTO)
        .andDo(createDocumentation(false));
  }

}
