package de.franken.appexamplebackend.controllers;

import de.franken.appexamplebackend.AbstractTest;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@SpringBootTest
class ResetControllerTest extends AbstractTest {

  @Test
  @DisplayName("Should send back OkDTO")
  void reset() throws Exception {
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/resetDB");
    mockMvc.perform(requestBuilder).andDo(print()).andExpect(OkDTO).andExpect(OkStatusCode).andDo(createDocumentation(false));
  }

}
