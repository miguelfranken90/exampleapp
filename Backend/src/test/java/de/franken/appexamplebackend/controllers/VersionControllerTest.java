package de.franken.appexamplebackend.controllers;

import de.franken.appexamplebackend.AbstractTest;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.hamcrest.Matchers.containsString;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
class VersionControllerTest extends AbstractTest {

  @Test
  @DisplayName("Should get a valid VersionDTO")
  void getVersion() throws Exception {
    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/version");

    mockMvc.perform(requestBuilder)
        .andDo(print())
        .andExpect(OkStatusCode)
        .andExpect(VersionDTO)
        .andDo(createDocumentation(false));
  }

}
