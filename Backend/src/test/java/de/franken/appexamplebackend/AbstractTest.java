package de.franken.appexamplebackend;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.franken.appexamplebackend.entities.Role;
import de.franken.appexamplebackend.entities.User;
import de.franken.appexamplebackend.repositories.RoleRepository;
import de.franken.appexamplebackend.repositories.UserRepository;
import de.franken.appexamplebackend.services.DatabaseResetService;
import de.franken.appexamplebackend.services.DummyDataService;
import de.franken.appexamplebackend.services.TokenAuthenticationService;
import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.RestDocumentationExtension;
import org.springframework.restdocs.mockmvc.MockMvcRestDocumentationConfigurer;
import org.springframework.restdocs.mockmvc.RestDocumentationResultHandler;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.restdocs.headers.HeaderDocumentation.headerWithName;
import static org.springframework.restdocs.headers.HeaderDocumentation.requestHeaders;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.*;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.prettyPrint;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;

@ExtendWith({ RestDocumentationExtension.class, SpringExtension.class})
@Log4j2
abstract public class AbstractTest {

  @Autowired
  UserRepository userRepository;

  @Autowired
  RoleRepository roleRepository;

  protected MockMvc mockMvc;

  private static Function<Boolean, RestDocumentationResultHandler> documentationHandler() {
    return (withAuthorization) -> {
      if (withAuthorization) {
        return document(
            "{ClassName}/{method-name}-{step}",
            preprocessRequest(prettyPrint()),
            preprocessResponse(prettyPrint()),
            requestHeaders(headerWithName("Authorization").description("Token"))
        );
      } else {
        return document(
            "{ClassName}/{method-name}-{step}",
            preprocessRequest(prettyPrint()),
            preprocessResponse(prettyPrint())
        );
      }
    };
  }

  protected static RestDocumentationResultHandler createDocumentation(boolean withAuthorizationHeader) {
    Function<Boolean, RestDocumentationResultHandler> myFunc = documentationHandler();
    return myFunc.apply(withAuthorizationHeader);
  }

  protected static String asJsonString(final Object obj) {
    try {
      return new ObjectMapper().writeValueAsString(obj);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  // region ResultMatchers
  // Status Codes
  protected static ResultMatcher OkStatusCode = MockMvcResultMatchers.status().isOk();
  protected static ResultMatcher NotFoundStatusCode = MockMvcResultMatchers.status().isNotFound();
  protected static ResultMatcher ConflictStatusCode = MockMvcResultMatchers.status().isConflict();
  protected static ResultMatcher UnauthorizedStatusCode = MockMvcResultMatchers.status().isUnauthorized();

  // DTOs
  protected static ResultMatcher OkDTO = MockMvcResultMatchers.content().string(containsString("OkDTO"));
  protected static ResultMatcher IdDTO = MockMvcResultMatchers.content().string(containsString("IdDTO"));
  protected static ResultMatcher ErrorDTO = MockMvcResultMatchers.content().string(containsString("ErrorDTO"));
  protected static ResultMatcher VersionDTO = MockMvcResultMatchers.content().string(containsString("VersionDTO"));
  protected static ResultMatcher PostDTO = MockMvcResultMatchers.content().string(containsString("PostDTO"));
  // endregion

  /**
   * Configures the MockMVC & adds dummy data to the database
   */
  @BeforeEach
  protected void beforeEachSetup(
      RestDocumentationContextProvider restDocumentation,
      @Autowired WebApplicationContext context,
      @Autowired DatabaseResetService databaseResetService,
      @Autowired DummyDataService dummyDataService) {
    // mock mvc configuration
    MockMvcRestDocumentationConfigurer docConfig = documentationConfiguration(restDocumentation);
    mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).apply(docConfig).build();

    // database reset & dummy data creation
    databaseResetService.reset();
    dummyDataService.createDummyData();
  }

  /**
   * Creates and returns a dummy token
   * @return Token associated with the newly created dummy user
   */
  public String createDummyToken() {
    return this.createDummyToken(new ArrayList<>());
  }

  public String createDummyToken(List<String> roles) {
    User user = new User("myDummyUsername", "a@b.com", "myPassword");

    roles.forEach(roleString -> {
      Role role = roleRepository.findByName(roleString);
      if (role != null) {
        user.addRole(role);
      } else {
        log.error("Cannot find role '" + role + "'!");
      }
    });

    userRepository.save(user);
    return TokenAuthenticationService.createToken(user.getName());
  }

  public String createDummyToken(String role) {
    return this.createDummyToken(Collections.singletonList(role));
  }

}
