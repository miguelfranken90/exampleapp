package de.franken.appexamplebackend.services;

import de.franken.appexamplebackend.entities.User;
import de.franken.appexamplebackend.repositories.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class TokenAuthenticationServiceTest {

  @Autowired
  private MockMvc mvc;

  @Autowired
  private UserRepository userRepository;

  @Test
  public void shouldNotAllowAccessToUnauthenticatedUsers() throws Exception {
    mvc.perform(MockMvcRequestBuilders.get("/users")).andExpect(status().isUnauthorized());
  }

  @Test
  public void nonexistentUserCannotGetToken() throws Exception {
    String username = "nonexistentuser";
    Assertions.assertFalse(userRepository.existsUserByName(username));

    String token = TokenAuthenticationService.createToken(username);
    assertNotNull(token);
    mvc.perform(MockMvcRequestBuilders.get("/users").header("Authorization", token)).andExpect(status().isUnauthorized());
  }

  @Test
  public void shouldGenerateAuthToken() throws Exception {
    User user = new User("myUsername", "a@b.com", "myPassword");
    userRepository.save(user);
    String token = TokenAuthenticationService.createToken(user.getName());

    assertNotNull(token);
    mvc.perform(MockMvcRequestBuilders.get("/users").header("Authorization", token)).andExpect(status().isOk());
  }

}
