package de.franken.appexamplebackend.dtos;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class ErrorDTOTest {

  @Test
  @DisplayName("Should set type attribute correctly")
  public void correctTypeTest() {
    ErrorDTO dto = new ErrorDTO();
    Assertions.assertEquals(dto.type, "ErrorDTO");
  }

}
