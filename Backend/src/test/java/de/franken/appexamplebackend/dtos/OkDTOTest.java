package de.franken.appexamplebackend.dtos;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class OkDTOTest {

  @Test
  @DisplayName("Should set type attribute correctly")
  public void correctTypeTest() {
    OkDTO dto = new OkDTO();
    Assertions.assertEquals(dto.type, "OkDTO");
  }

}
