package de.franken.appexamplebackend.dtos;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class VersionDTOTest {

  @Test
  @DisplayName("Should set type attribute correctly")
  public void correctTypeTest() {
    VersionDTO dto = new VersionDTO("0.0.1-SNAPSHOT");
    Assertions.assertEquals(dto.type, "VersionDTO");
  }

}
