package de.franken.appexamplebackend.dtos;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class IdDTOTest {

  @Test
  @DisplayName("Should set type attribute correctly")
  public void correctTypeTest() {
    IdDTO dto = new IdDTO(0L);
    assertEquals(dto.type, "IdDTO");
  }

  @Test
  @DisplayName("Should set id correctly")
  public void setIdTest() {
    long id = 345L;
    IdDTO dto = new IdDTO(id);
    assertEquals(dto.getId(), id);
  }

}
