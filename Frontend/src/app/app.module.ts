import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AngularMaterialModule } from './shared/angular-material.module';
import { NavComponent } from '@layout/nav/nav.component';
import { AppRouting } from './app.routing';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { FlexModule } from '@angular/flex-layout';
import { MainLayoutComponent } from '@layout/main/main-layout.component';
import { AuthGuard } from './shared/guards/auth-guard';
import { BasicAuthHttpInterceptor } from './shared/interceptors/basic-auth-htpp-interceptor.service';
import { AuthModule } from './pages/auth/auth.module';
import { environment } from '../environments/environment';
import { translocoLoader } from './transloco.loader';
import { TranslocoModule, TRANSLOCO_CONFIG, TranslocoConfig } from '@ngneat/transloco';
import { LoadingBarHttpClientModule } from '@ngx-loading-bar/http-client';
import { LoadingBarModule } from '@ngx-loading-bar/core';

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    MainLayoutComponent
  ],
  imports: [
    BrowserModule,
    AppRouting,
    BrowserAnimationsModule,
    AngularMaterialModule,
    HttpClientModule,
    FlexModule,
    AuthModule.forRoot(),
    TranslocoModule,
    LoadingBarHttpClientModule,
    LoadingBarModule
  ],
  bootstrap: [AppComponent],
  providers: [
    AuthGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: BasicAuthHttpInterceptor,
      multi: true
    },
    {
      provide: TRANSLOCO_CONFIG,
      useValue: {
        listenToLangChange: true,
        defaultLang: 'en',
        availableLangs: ['en', 'de'],
        fallbackLang: 'en',
        prodMode: environment.production
      } as TranslocoConfig
    },
    translocoLoader

  ]
})
export class AppModule { }
