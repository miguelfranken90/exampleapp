import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './shared/guards/auth-guard';
import { MainLayoutComponent } from '@layout/main/main-layout.component';
import { AuthComponent } from './pages/auth/auth.component';

const routes: Routes = [
  {
    path: '',
    component: MainLayoutComponent,
    canActivateChild: [AuthGuard],
    children: [
      {
        path: 'users',
        loadChildren: () => import('./pages/user/user.module').then(m => m.UserModule),
      },
      {
        path: 'employees',
        loadChildren: () => import('./pages/employee/employee.module').then(m => m.EmployeeModule)
      },
      {
        path: 'beispiel',
        loadChildren: () => import('./pages/beispiel/beispiel.module').then(m => m.BeispielModule)
      },
      {
        path: 'more',
        loadChildren: () => import('./pages/more/more.module').then(m => m.MoreModule)
      },
      {
        path: '',
        redirectTo: '/users',
        pathMatch: 'full'
      },
    ]
  },
  {
    path: 'auth',
    component: AuthComponent,
    loadChildren: () => import('./pages/auth/auth.module').then(m => m.AuthModule)
  },
  {
    path: '**',
    redirectTo: 'users/overview'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRouting { }
