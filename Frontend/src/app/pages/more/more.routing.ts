import { Routes, RouterModule } from '@angular/router';

import { MoreComponent } from './more.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'more',
    pathMatch: 'full'
  },
  {
    path: 'more',
    component: MoreComponent,
  },

  {
    path: '**',
    component: MoreComponent
  }
];

export const routing = RouterModule.forChild(routes);
