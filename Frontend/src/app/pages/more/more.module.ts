import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MoreComponent } from './more.component';
import {routing} from "./more.routing";


@NgModule({
  declarations: [MoreComponent],
  imports: [
    CommonModule,
    routing
  ]
})
export class MoreModule { }
