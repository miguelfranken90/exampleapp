import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '@services/user.service';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss']
})
export class UserFormComponent {

  // region Form
  public userForm = this.fb.group({
    name: ['', Validators.required],
    email: ['', [Validators.required, Validators.email]]
  });

  get name() { return this.userForm.get('name'); }
  get email() { return this.userForm.get('email'); }

  getEmailErrorMessage() {
    return this.userForm.get('email').hasError('required') ? 'You must enter a value' :
      this.userForm.get('email').hasError('email') ? 'Not a valid email' :
        '';
  }

  getNameErrorMessage() {
    return this.userForm.get('name').hasError('required') ? 'You must enter a value' : '';
  }
  // endregion

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private userService: UserService) {
  }

  onSubmit() {
    this.userService.save(this.userForm.value).subscribe(_ => {
      this.userForm.reset();
      this.gotoUserList();
    });
  }

  gotoUserList() {
    this.router.navigate(['/users']);
  }

}
