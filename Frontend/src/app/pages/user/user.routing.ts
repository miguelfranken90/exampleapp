import { Routes, RouterModule } from '@angular/router';
import { UserComponent } from './user.component';
import { UserDetailsComponent } from './user-details/user-details.component';

const routes: Routes = [
    {
    path: '',
    redirectTo: 'overview',
    pathMatch: 'full'
  },
  {
    path: 'overview',
    component: UserComponent,
  },
  {
    path: 'details/:id',
    component: UserDetailsComponent
  },
  {
    path: '**',
    component: UserComponent
  }
];

export const routing = RouterModule.forChild(routes);
