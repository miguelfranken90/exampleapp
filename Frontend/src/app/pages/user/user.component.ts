import { Component, OnInit } from '@angular/core';
import { UserService } from '@services/user.service';
import { RightClickMenuEntry, TableCell, TableColumn, TableRow, TableSettings } from '@components/table/table.types';
import { Observable } from 'rxjs';
import { UserDTO } from '@dtos/user.dto';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-overview',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  public columns: TableColumn[] = [
    {
      id: 'name',
      name: 'Name',
      prop: 'name',
      visible: true,
      sortable: true,
    },
    {
      id: 'email',
      name: 'E-Mail',
      prop: 'email',
      visible: true,
      sortable: true,
    },
    {
      id: 'disabled',
      name: 'Disabled',
      prop: 'disabled',
      visible: true,
      sortable: true,
      cellType: 'checkbox'
    }
  ];

  public rightClickMenu: RightClickMenuEntry[] = [
    {
      label: 'myLabel',
      function: (cell: TableCell) => console.log('CLICKED CELL', cell)
    },
    {
      label: 'myLabel2',
      function: (cell: TableCell) => console.log('CLICKED CELL 2', cell)
    }
  ];

  public tableSettings: TableSettings = {
    filterable: false,
    deletable: false,
    allowBatchMode: false,
    sortable: true,
    editable: false,
    rowClick: this.onClickRow.bind(this),
    rowCursor: 'pointer'
  };

  public data: Observable<UserDTO[]>;

  constructor(private userService: UserService, private router: Router) { }

  ngOnInit() {
    this.data = this.userService.findAll();
  }

  private onClickRow(row: TableRow) {
    console.log('CLICKED ROW', row);
    const dto: UserDTO = row.data;
    this.router.navigateByUrl(`/users/details/${dto.id}`);
  }

}
