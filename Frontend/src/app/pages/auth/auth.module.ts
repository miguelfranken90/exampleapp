import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthComponent } from './auth.component';
import { LoginComponent } from './login/login.component';
import { LoginCardComponent } from './login/login-card/login-card.component';
import { LoginTitleComponent } from './login/login-title/login-title.component';
import { routing } from './auth.routing';
import { AngularMaterialModule } from '../../shared/angular-material.module';
import { HttpClientModule } from '@angular/common/http';
import { FlexModule } from '@angular/flex-layout';
import { RegisterComponent } from './register/register.component';
import { RegisterCardComponent } from './register/register-card/register-card.component';
import { AuthCardComponent } from './auth-card/auth-card.component';
import { TranslocoModule } from '@ngneat/transloco';

@NgModule({
  declarations: [
    AuthComponent,
    LoginComponent,
    LoginCardComponent,
    LoginTitleComponent,
    RegisterComponent,
    RegisterCardComponent,
    AuthCardComponent
  ],
  imports: [
    CommonModule,
    AngularMaterialModule,
    HttpClientModule,
    FlexModule,
    routing,
    TranslocoModule
  ]
})
export class AuthModule {
  static forRoot(): ModuleWithProviders<AuthModule> {
    return {
      ngModule: AuthModule
    };
  }
}
