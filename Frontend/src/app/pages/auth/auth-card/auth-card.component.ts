import { Component, Input, OnInit } from '@angular/core';
import { TranslocoService } from '@ngneat/transloco';
import { Locale } from '../locale';

@Component({
  selector: 'app-auth-card',
  templateUrl: './auth-card.component.html',
  styleUrls: ['./auth-card.component.scss']
})
export class AuthCardComponent implements OnInit {

  @Input()
  public title: string;

  @Input()
  public subtitle: string;

  constructor(private translocoService: TranslocoService) {
  }

  ngOnInit() {
  }

  public switchLanguage(locale: Locale) {
    this.translocoService.setActiveLang(locale);
  }

  public isLanguageActive(locale: Locale) {
    return this.translocoService.getActiveLang() === locale;
  }

}
