import { Component, Input, OnInit } from '@angular/core';
import {Rezept,Zutat} from "../beispiel.component";
@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {

  @Input()
  rezept: Rezept;

  @Input()
  numberOfPersons: number= 1;

  @Input()
  sum: number= 1;


  constructor() { }

  ngOnInit(): void {
  }

}
