import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

export interface Zutat {
  name: string;
  quantity: number;
  price: number;
}

export interface Rezept {
  name: string;
  zutaten: Zutat[];
  dauer: number;
  author: Author;
}

export interface Author {
  name: string;
  alter: number;
  ort: string;
}

@Component({
  selector: 'app-beispiel',
  templateUrl: './beispiel.component.html',
  styleUrls: ['./beispiel.component.scss']
})
export class BeispielComponent implements OnInit {

  currentlySelectedRecipe: Rezept;

  currentlySelectedRecipes: Rezept[] = [];

  zutat1: Zutat ={
    name: "Zutat1",
    quantity: 1,
    price: 1
  };

  zutat2: Zutat ={
    name: "Zutat2",
    quantity: 2,
    price: 2
  };

  zutat3: Zutat ={
    name: "Zutat3",
    quantity: 3,
    price:3
  };

  author1: Author = {
    name: "Miguel",
    alter: 23,
    ort: "Deutschland"
  };

  author2: Author = {
    name: "Kim",
    alter: 22,
    ort: "Deutschland"
  };

  // Rezept -> Autor

  // Autor1 -> Rezept
  // -> rezept1
  // -> rezept2

  rezept1: Rezept = {
    name: "Pizza",
    zutaten: [
      this.zutat1
    ],
    dauer: 20,
    author: this.author1
  };

  rezept2: Rezept = {
    name: "Suppe",
    zutaten: [
      this.zutat2,
      this.zutat3
    ],
    dauer: 10,
    author: this.author1
  };

  rezept3: Rezept = {
    name: "Curry",
    zutaten:[
      this.zutat3
    ],
    dauer: 30,
    author: this.author2
  };

  rezepte = [
    this.rezept1, // Item 0
    this.rezept2, // Item 1
    this.rezept3 // Item 2
  ];

  suppe: string = "Suppe";

  // Array
  liste = [
    this.suppe,
    "Curry",
    "Pizza"
  ];


  public getSum(rezept: Rezept) {
    if (rezept) {
       let sum = 0;
       let numberOfPersons = this.getNumberOfPersons();
       rezept.zutaten.forEach((zutat: Zutat) => {
          sum += zutat.quantity * zutat.price * numberOfPersons;
       });
       return sum;
    } else {
       return 0;
    }

  }
  public getSumPerPerson(rezept:Rezept){
  let sum = 0;

  rezept.zutaten.forEach((zutat: Zutat) => {
  sum += zutat.quantity * zutat.price;
  });
 return sum;
 }

  constructor(private fb: FormBuilder) {
    console.log("Recipes for author 2", this.getRecipes(this.author1), "am ende");
  }

  ngOnInit(): void {
    this.group = this.fb.group({
      persons: ['', Validators.required],
      max: ['', Validators.required]
    });

    this.group.get("persons").setValue(2);
    this.group.get("max").setValue(100);
  }

  public group: FormGroup;

  public getNumberOfPersons(): number {
    return this.group.get("persons").value;
  }

  public getMaximalenPreisProPerson() {
    let value = this.group.get("max").value;
  // Resetting/deselecting currentlySelectedRecipe
    if (value <= 0){
    this.currentlySelectedRecipe= null;
    }


    return value;
  }


  /**
   * Wird ausgefrührt wenn man auf eienne der Rezeot-Buttons drückt
   * @param rezept
   */
  public onClickRezept(rezept: Rezept) {
    this.currentlySelectedRecipe = rezept;
    this.currentlySelectedRecipes = [];
    this.currentlySelectedRecipes.push(rezept);
    this.selectedAuthor = rezept.author;
    console.log("Clicked recipe");
  }

  public getRecipes(author: Author) {
    return this.rezepte.filter((rezept) => {
      return rezept.author == author; // true oder false
    });
  }

  public findAllAuthors(): Author[] {
    let authors = [];

    this.rezepte.forEach((rezept: Rezept) => {
      let item = rezept.author;

      let alreadyContainsTheAuthor: boolean = authors.includes(item);
      if (alreadyContainsTheAuthor) {
        // console.log("Already contained author " + item.name);
      } else {
        authors.push(item);
      }
    });

    return authors;
  }

  public selectedAuthor: Author = undefined;

  public onClickAuthor(author: Author) {
    this.selectedAuthor = author;
    console.log("Clicked an author", author);
    console.log("Found recipes:", this.getRecipes(author));
    this.currentlySelectedRecipes = this.getRecipes(author);
  }

}
