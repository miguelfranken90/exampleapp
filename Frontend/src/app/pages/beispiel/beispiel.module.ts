import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BeispielComponent } from './beispiel.component';
import { routing } from './beispiel.routing';
import { ReactiveFormsModule } from "@angular/forms";
import { InputModule } from "../../shared/input/input.module";
import { FlexModule } from "@angular/flex-layout";
import { DetailsComponent } from './details/details.component';
import { AngularMaterialModule } from "../../shared/angular-material.module";
import { AuthorCardComponent } from './author-card/author-card.component';

@NgModule({
  declarations: [BeispielComponent, DetailsComponent, AuthorCardComponent],
  imports: [
    CommonModule,
    routing,
    ReactiveFormsModule,
    InputModule,
    FlexModule,
    AngularMaterialModule
  ]
})
export class BeispielModule { }
