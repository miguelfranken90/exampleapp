import { Routes, RouterModule } from '@angular/router';

import { BeispielComponent } from './beispiel.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'beispiel',
    pathMatch: 'full'
  },
  {
    path: 'beispiel',
    component: BeispielComponent,
  },

  {
    path: '**',
    component: BeispielComponent
  }
];

export const routing = RouterModule.forChild(routes);
