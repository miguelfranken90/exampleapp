import { TableService } from '@components/table/table.types';
import { EmployeeDTO } from '@dtos/employee.dto';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { EmployeeService } from '@services/employee.service';

@Injectable({
  providedIn: 'root'
})
export class EmployeeTableService implements TableService<EmployeeDTO> {

  constructor(private employeeService: EmployeeService) {
  }

  findAll(): Observable<EmployeeDTO[]> {
    return this.employeeService.findAll();
  }

}
