import { Component, EventEmitter, Output } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { EmployeeService } from '@services/employee.service';
import { Logger } from '@upe/logger';

@Component({
  selector: 'app-employee-form',
  templateUrl: './employee-form.component.html',
  styleUrls: ['./employee-form.component.scss']
})
export class EmployeeFormComponent {

  @Output()
  public update = new EventEmitter();

  private logger: Logger = new Logger({ name: 'EmployeeFormComponent', flags: ['component'] });

  // region Form
  public employeeForm = this.fb.group({
    name: ['', Validators.required],
    designation: ['', [Validators.required]],
    salary: ['', [Validators.required]],
  });

  get name() { return this.employeeForm.get('name'); }
  get designation() { return this.employeeForm.get('designation'); }
  get salary() { return this.employeeForm.get('salary'); }

  getNameErrorMessage() {
    return this.name.hasError('required') ? 'You must enter a value' : '';
  }

  getDesignationErrorMessage() {
    return this.designation.hasError('required') ? 'You must enter a value' : '';
  }

  getSalaryErrorMessage() {
    return this.salary.hasError('required') ? 'You must enter a value' : '';
  }
  // endregion

  constructor(
    private fb: FormBuilder,
    private employeeService: EmployeeService) {
  }

  onSubmit() {
    this.logger.info('Submit', this.employeeForm.value);
    this.employeeService.save(this.employeeForm.value).subscribe(_ => {
      // this.dataSource.update();
      this.employeeForm.reset();
      this.update.emit();
    }, (error) => this.logger.error('Cannot save new employee', error));
  }

}
