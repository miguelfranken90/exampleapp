import { Routes, RouterModule } from '@angular/router';
import { EmployeeComponent } from './employee.component';

const routes: Routes = [
    {
    path: '',
    redirectTo: 'overview',
    pathMatch: 'full'
  },
  {
    path: 'overview',
    component: EmployeeComponent,
  },
  {
    path: '**',
    component: EmployeeComponent }
];

export const routing = RouterModule.forChild(routes);
