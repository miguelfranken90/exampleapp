import { Component, OnInit } from '@angular/core';
import { RightClickMenuEntry, TableCell, TableColumn, TableSettings } from '@components/table/table.types';
import { EmployeeTableService } from './employee-table.service';
import { Observable } from 'rxjs';
import { EmployeeDTO } from '@dtos/employee.dto';
import { EmployeeService } from '@services/employee.service';
import { EmployeeUpdateDTO } from '@dtos/employee-update.dto';
import { Validators } from '@angular/forms';
import { Token } from '../../shared/token';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {

  public columns: TableColumn[] = [
    {
      id: 'name',
      name: 'Name',
      prop: 'name',
      visible: true,
      validators: [
        Validators.required
      ],
      sortable: true,
      input: {
        type: 'text'
      },
      footer: _ => '∑'
    },
    {
      id: 'designation',
      name: 'Designation',
      prop: 'designation',
      visible: true,
      validators: [
        Validators.required
      ],
      input: {
        type: 'autocomplete',
        options: ['Student', 'Professor']
      }
    },
    {
      id: 'salary',
      name: 'Salary',
      prop: 'salary',
      visible: this.returnTrue(),
      footer: (data) => this.getFooter(data),
      validators: [
        Validators.required,
        Validators.pattern(/^-?(0|[1-9]\d*)?$/),
        Validators.max(1000)
      ],
      input: {
        type: 'text'
      }
    }
  ];

  public rightClickMenu: RightClickMenuEntry[] = [
    {
      label: 'myLabel',
      function: (cell: TableCell) => console.log('CLICKED CELL', cell)
    },
    {
      label: 'myLabel2',
      function: (cell: TableCell) => console.log('CLICKED CELL 2', cell)
    }
  ];

  public tableSettings: TableSettings = {
    filterable: true,
    deletable: Token.hasRole('ROLE_ADMIN'),
    allowBatchMode: false,
    sortable: true,
    editable: true,
    // rowClick: (row: TableRow) => console.log("CLICKED ROW", row)
  };

  public data: Observable<EmployeeDTO[]>;

  constructor(
    public employeeTableService: EmployeeTableService,
    private employeeService: EmployeeService) { }

  ngOnInit() {
    this.data = this.employeeTableService.findAll();
  }

  private returnTrue() {
    return true;
  }

  private getFooter(dtos: EmployeeDTO[]) {
    let sum = 0;
    dtos.forEach((dto) => sum += dto.salary);
    return '' + sum;
  }

  public onTableUpdate() {
    this.data = this.employeeTableService.findAll();
  }

  public onDelete(event) {
    this.employeeService.delete(event).subscribe(() => {
      this.data = this.employeeTableService.findAll();
    });
  }

  public onSave(event) {
    const dto: EmployeeUpdateDTO = new EmployeeUpdateDTO();
    dto.empId = event.id;
    dto.designation = event.data.designation;
    dto.name = event.data.name;
    dto.salary = event.data.salary;

    this.employeeService.update(dto).subscribe(_ => {
      this.data = this.employeeTableService.findAll();
    }, (error) => console.error('CANNOT SAVE', error));
  }

}
