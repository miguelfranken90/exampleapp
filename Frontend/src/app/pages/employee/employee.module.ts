import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmployeeComponent } from './employee.component';
import { routing } from './employee.routing';
import { AngularMaterialModule } from '../../shared/angular-material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EmployeeFormComponent } from './employee-form/employee-form.component';
import { TableModule } from '@components/table/table.module';
import { InputModule } from '../../shared/input/input.module';

@NgModule({
  declarations: [
    EmployeeComponent,
    EmployeeFormComponent
  ],
  imports: [
    CommonModule,
    AngularMaterialModule,
    FlexLayoutModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    TableModule,
    routing,
    InputModule
  ]
})
export class EmployeeModule { }
