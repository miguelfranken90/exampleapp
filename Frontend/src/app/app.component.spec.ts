import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { LoadingBarModule } from '@ngx-loading-bar/core';
import { AppRouting } from './app.routing';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AngularMaterialModule } from './shared/angular-material.module';
import { HttpClientModule } from '@angular/common/http';
import { FlexModule } from '@angular/flex-layout';
import { AuthModule } from './pages/auth/auth.module';
import { TranslocoModule } from '@ngneat/transloco';
import { LoadingBarHttpClientModule } from '@ngx-loading-bar/http-client';
import { NavComponent } from '@layout/nav/nav.component';
import { MainLayoutComponent } from '@layout/main/main-layout.component';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        NavComponent,
        MainLayoutComponent
      ],
      imports: [
        BrowserModule,
        AppRouting,
        BrowserAnimationsModule,
        AngularMaterialModule,
        HttpClientModule,
        FlexModule,
        AuthModule.forRoot(),
        TranslocoModule,
        LoadingBarHttpClientModule,
        LoadingBarModule
      ]
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });
});
