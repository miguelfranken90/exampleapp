import { Component, Input, OnInit } from '@angular/core';
import { AbstractInputComponent } from '../abstract.input';

export type FormControlName = string;

@Component({
  selector: 'app-autocomplete',
  templateUrl: './autocomplete.component.html',
  styleUrls: ['./autocomplete.component.scss']
})
export class AutocompleteComponent extends AbstractInputComponent implements OnInit {

  @Input()
  public options: string[];

  constructor() {
    super();
  }

  ngOnInit(): void {
  }

}
