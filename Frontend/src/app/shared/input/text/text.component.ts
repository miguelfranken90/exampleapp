import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { AbstractInputComponent } from '../abstract.input';
import { MatTooltip } from '@angular/material/tooltip';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-text',
  templateUrl: './text.component.html',
  styleUrls: ['./text.component.scss']
})
export class TextComponent extends AbstractInputComponent implements OnInit {

  @Input()
  public label: string;

  @ViewChild('tooltip', {static: true}) tooltip: MatTooltip;

  constructor() {
    super();
  }

  ngOnInit(): void {
    this.group.get(this.name).statusChanges.pipe(filter((status) => status === 'INVALID')).subscribe(_ => {
      this.tooltip.show();
    });
  }

}
