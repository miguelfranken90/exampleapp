import { Input } from '@angular/core';
import { AbstractControl, FormGroup } from '@angular/forms';
import { FormControlName } from './autocomplete/autocomplete.component';

export abstract class AbstractInputComponent {

  @Input()
  public group: FormGroup;

  @Input()
  public name: FormControlName;

  /**
   * Returns whether the value of the input field is valid or not.
   */
  public isFormInputInvalid(): boolean {
    return this.group.get(this.name).invalid;
  }

  /**
   * Returns an error string in case the input field is not valid.
   * Calling this method for valid input will return an empty string.
   */
  public getFormInputErrorMessage(): string {
    const control: AbstractControl = this.getFormControl();
    let error = '';
    if (control.hasError('required')) {
      error = 'This field is required!'
    } else if (control.hasError('pattern')) {
      error = 'Must be a number!'
    } else if (control.hasError('max')) {
      error = `Maximum value is ${control.errors.max.max}`;
    }
    return error;
  }

  public getFormControl(): AbstractControl {
    return this.group.get(this.name);
  }

}
