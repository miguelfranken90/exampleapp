import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TextComponent } from './text/text.component';
import { AutocompleteComponent } from './autocomplete/autocomplete.component';
import { AngularMaterialModule } from '../angular-material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { FocusInputDirective } from '../directives/focus-input.directive';



@NgModule({
  declarations: [TextComponent, AutocompleteComponent, FocusInputDirective],
  exports: [
    AutocompleteComponent,
    TextComponent,
    FocusInputDirective
  ],
  imports: [
    CommonModule,
    AngularMaterialModule,
    ReactiveFormsModule
  ]
})
export class InputModule { }
