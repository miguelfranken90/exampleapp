import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanActivateChild, Router, RouterStateSnapshot } from '@angular/router';
import { AuthenticationService } from '@services/authentication.service';
import { Observable } from 'rxjs';
import { Logger } from '@upe/logger';
import { map } from 'rxjs/operators';

@Injectable()
export class AuthGuard implements CanActivate, CanActivateChild {

  private logger: Logger = new Logger({ name: 'AuthGuard', flags: ['guard'] });

  constructor(private authenticationService: AuthenticationService, private router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    const url: string = state.url;
    return this.checkLogin(url);
  }

  canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.canActivate(childRoute, state);
  }

  checkLogin(url: string): Observable<boolean> {
    return this.authenticationService.isUserLoggedIn().pipe(map((loggedIn: boolean) => {
      if (!loggedIn) {
        // save attempted URL for redirecting
        this.authenticationService.redirectUrl = url;

        this.logger.info('User is not logged in. Redirecting to login page..');
        this.router.navigate(['/auth/login']);
      }
      return loggedIn;
    }));
  }

}
