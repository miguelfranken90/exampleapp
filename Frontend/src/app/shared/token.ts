import * as jwt_decode from 'jwt-decode';

export type Role = 'ROLE_ADMIN' | 'ROLE_USER';
export type Privilege = string;

export interface Authority {
  authority: string
}

export class Token {

  public static hasRole(role: Role): boolean {
    const authorities = Token.getAuthorities();
    console.log('AUTHORITIES', authorities);
    return !!authorities.find((authority) => authority.authority === role);
  }

  public static hasPrivilege(privilege: Privilege): boolean {
    const authorities = Token.getAuthorities();
    return !!authorities.find((authority) => authority.authority === privilege);
  }

  public static isAdmin(): boolean {
    return Token.hasRole('ROLE_ADMIN');
  }

  private static getAuthorities(): Authority[] {
    const token = sessionStorage.getItem('token');
    const decoded = jwt_decode(token);
    return decoded.roles;
  }

}
