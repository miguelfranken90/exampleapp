import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UserDTO } from '@dtos/user.dto';
import { HttpService } from '@services/http.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private readonly usersUrl: string;

  constructor(private http: HttpService) {
    this.usersUrl = '/users';
  }

  public findAll(): Observable<UserDTO[]> {
    return this.http.get<UserDTO[]>(this.usersUrl);
  }

  public save(user: UserDTO) {
    return this.http.post<UserDTO>(this.usersUrl, user);
  }

  public delete(user: UserDTO) {
    return this.http.delete<UserDTO>(`/users/${user.id}`);
  }

}
