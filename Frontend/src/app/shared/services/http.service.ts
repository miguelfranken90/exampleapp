import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { environment } from '../../../environments/environment';
import { catchError } from 'rxjs/operators';
import { Logger } from '@upe/logger';

interface Request {
  url: string
}

interface PostRequest extends Request {
  body?: string
}

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  private logger: Logger = new Logger({ name: 'HttpService', flags: ['service'] });

  private apiUrl = environment.api;

  constructor(private http: HttpClient) { }

  public post<T>(url: string, body: any | null): Observable<T> {
    const postRequest: PostRequest = {
      url: this.apiUrl + url,
      body
    };
    return this.http.post<T>(postRequest.url, postRequest.body).pipe(
      catchError(this.getErrorCallback(postRequest).bind(this))
    );
  }

  public get<T>(url: string): Observable<T> {
    const getRequest: Request = {
      url: this.apiUrl + url,
    };

    return this.http.get<T>(getRequest.url).pipe(
      catchError(this.getErrorCallback(getRequest).bind(this))
    );
  }

  public delete<T>(url: string): Observable<T> {
    const deleteRequest: Request = {
      url: this.apiUrl + url
    };
    return this.http.delete<T>(deleteRequest.url).pipe(
      catchError(this.getErrorCallback(deleteRequest).bind(this))
    );
  }

  /**
   * Logs received errors from the backend
   * @param request The meta-data of the request that was sent to the backend
   */
  private getErrorCallback(request: Request) {
    return (fullHttpErrorResponse: HttpErrorResponse) => {
      const errorDto = fullHttpErrorResponse.error;
      this.logger.error('Error occurred', {
        request,
        error: errorDto
      });

      return throwError(errorDto);
    }
  }

}
