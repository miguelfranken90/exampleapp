import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { EmployeeDTO } from '@dtos/employee.dto';
import { HttpService } from '@services/http.service';
import { Logger } from '@upe/logger';
import { EmployeeCreationDTO } from '@dtos/employee-creation.dto';
import { EmployeeUpdateDTO } from '@dtos/employee-update.dto';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  private logger: Logger = new Logger({ name: 'EmployeeService', flags: ['service'] });

  constructor(private http: HttpService) { }

  public findAll(): Observable<EmployeeDTO[]> {
    this.logger.info('Find all employees');
    return this.http.get<EmployeeDTO[]>('/employees');
  }

  public delete(employee: EmployeeDTO) {
    this.logger.info('Deleting employee..', employee);
    return this.http.delete<EmployeeDTO>(`/employees/${employee.empId}`);
  }

  public save(employee: EmployeeCreationDTO) {
    this.logger.info('Saving employee..', employee);
    return this.http.post<EmployeeDTO>('/employees', employee);
  }

  public update(employee: EmployeeUpdateDTO) {
    this.logger.info('Updating employee..', employee);
    return this.http.post('/employees/update', employee);
  }

}
