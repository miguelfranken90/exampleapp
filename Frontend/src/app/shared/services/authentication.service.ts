import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { catchError, map } from 'rxjs/operators';
import { HttpService } from '@services/http.service';
import { Logger } from '@upe/logger';
import { MatSnackBar } from '@angular/material/snack-bar';
import { HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private logger: Logger = new Logger({ name: 'AuthenticationService', flags: ['service'] });

  constructor(
    private snackBar: MatSnackBar,
    private router: Router,
    private http: HttpService) { }

  private _redirectUrl = '';
  public set redirectUrl(url: string) {
    if (!url.match('login')) {
      this._redirectUrl = url;
    }
  }

  public authenticate(username, password) {
    const req = this.http.post<any>('/authenticate', {username, password}).pipe(
      map(
        userData => {
          sessionStorage.setItem('username', username);
          const tokenStr = 'Bearer ' + userData.token;
          sessionStorage.setItem('token', tokenStr);
          return userData;
        }
      )
    );

    req.subscribe(_ => {
      this.logger.info('Valid login');
      this.router.navigate([this._redirectUrl]);
    }, (fullErrorResponse: HttpErrorResponse) => {
      this.snackBar.open('Sie konnten nicht angemeldet werden! Überprüfen Sie bitte Ihre Eingabe!', '', {
        duration: 3000
      });
      this.logger.error('Invalid login', fullErrorResponse.error);
    });
  }

  public isUserLoggedIn(): Observable<boolean> {
    const user = sessionStorage.getItem('username');
    let token = sessionStorage.getItem('token');
    if (user === null || token === null) {
      this.logger.error('No user and/or token data found');
      return of(false);
    }
    token = token.substr(7, token.length);
    return this.http.post('/validate', token).pipe(map(() => true), catchError(() => of(false)));
  }

  logOut() {
    sessionStorage.removeItem('username');
    sessionStorage.removeItem('token');
    this.router.navigateByUrl('/auth/login');
  }

  public register(username, email, password) {
    const user: any = {
      name: username,
      email,
      password
    };

    this.http.post<any>('/register', user)
      .subscribe(res => this.logger.info('User registered sucessfully', res), error => {
        this.logger.error('cannot register user', error);
        this.snackBar.open('Registrierung nicht erfolgreich! Überprüfen Sie bitte Ihre Eingabe!');
      });
  }
}
