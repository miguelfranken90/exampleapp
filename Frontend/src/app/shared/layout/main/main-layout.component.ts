import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { VersionDTO } from '@dtos/version.dto';
import { HttpService } from '@services/http.service';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-main',
  templateUrl: './main-layout.component.html',
  styleUrls: ['./main-layout.component.scss']
})
export class MainLayoutComponent implements OnInit {

  public version: Observable<string>;

  constructor(private http: HttpService) { }

  ngOnInit() {
    this.getVersion();
  }

  public getVersion() {
    this.version = this.http.get<VersionDTO>('/version').pipe(map(dto => {
      return `${dto.version}  (<a href="https://gitlab.com/miguelfranken90/exampleapp/-/commit/${dto.commitId}">${dto.branch}/${dto.commitId}</a>)`;
    }));
  }

}
