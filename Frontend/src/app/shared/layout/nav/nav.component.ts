import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '@services/authentication.service';
import { HttpService } from '@services/http.service';
import { Logger } from '@upe/logger';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {

  private logger: Logger = new Logger({ name: 'NavComponent', flags: ['component'] });

  constructor(private authenticationService: AuthenticationService, private http: HttpService) { }

  ngOnInit() {
  }

  /**
   * Logs out the user and redirects him to the login page
   */
  public logout() {
    this.authenticationService.logOut();
  }

  /**
   * Requests the backend to create dummy data
   */
  public createDummyData() {
    this.logger.info('Creating dummy data..');
    this.http.get<string>('/createDummyData').subscribe((data) => {
      this.logger.info('Dummy data created');
      window.location.reload();
    });
  }

  /**
   * Requests the backend to reset the database
   */
  public resetDatabase() {
    this.logger.info('Resetting database');
    this.http.get<string>('/resetDB').subscribe(_ => {
      this.logger.info('Database reset was successful');
      window.location.reload();
    });
  }

}
