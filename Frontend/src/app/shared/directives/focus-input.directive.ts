import { AfterViewInit, ChangeDetectorRef, Directive, ElementRef, Input } from '@angular/core';

@Directive({
  selector: '[appFocusInput]'
})
export class FocusInputDirective implements AfterViewInit {

  @Input()
  private appFocusInput = false;

  constructor(
    private el: ElementRef,
    private cdRef: ChangeDetectorRef
  ) { }

  ngAfterViewInit(): void {
    if (this.appFocusInput) {
      const input: HTMLInputElement = this.el.nativeElement.querySelector('input');
      if (input) {
        input.focus();
        this.cdRef.detectChanges();
      }
    }
  }

}
