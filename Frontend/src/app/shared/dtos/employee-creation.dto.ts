export class EmployeeCreationDTO {
  public name: string;
  public designation: string;
  public salary: string;
}
