export class EmployeeUpdateDTO {
  public empId: number;
  public name: string;
  public designation: string;
  public salary: string;
}
