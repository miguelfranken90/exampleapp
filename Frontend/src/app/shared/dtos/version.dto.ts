export class VersionDTO {
  version: string;
  branch: string;
  commitId: string;
}
