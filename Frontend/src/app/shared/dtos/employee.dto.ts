export class EmployeeDTO {
  public empId: number;
  public name: string;
  public designation: string;
  public salary: number;
}
