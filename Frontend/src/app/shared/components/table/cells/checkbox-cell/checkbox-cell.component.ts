import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-checkbox-cell',
  templateUrl: './checkbox-cell.component.html',
  styleUrls: ['./checkbox-cell.component.scss']
})
export class CheckboxCellComponent implements OnInit {

  @Input()
  public checked: boolean;

  constructor() { }

  ngOnInit(): void {
  }

}
