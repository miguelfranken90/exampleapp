import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TableComponent } from './table.component';
import { AngularMaterialModule } from '../../angular-material.module';
import { MatSortModule } from '@angular/material/sort';
import { ReactiveFormsModule } from '@angular/forms';
import { FlexModule } from '@angular/flex-layout';
import { InputModule } from '../../input/input.module';
import { CheckboxCellComponent } from './cells/checkbox-cell/checkbox-cell.component';



@NgModule({
  declarations: [TableComponent, CheckboxCellComponent],
  imports: [
    CommonModule,
    AngularMaterialModule,
    MatSortModule,
    ReactiveFormsModule,
    FlexModule,
    InputModule
  ],
  exports: [TableComponent]
})
export class TableModule { }
