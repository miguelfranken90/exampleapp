import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { RightClickMenu, TableCell, TableColumn, TableColumnIdentifier, TableRow, TableRowIndex, TableSettings } from '@components/table/table.types';
import { EmployeeDTO } from '@dtos/employee.dto';
import { Observable } from 'rxjs';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { AbstractControlOptions, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { SelectionModel } from '@angular/cdk/collections';
import { MatMenuTrigger } from '@angular/material/menu';
import { ValidatorFn } from '@angular/forms';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {

  public hasAlreadyAnimated = false;

  public hasData = true;

  get data(): Observable<any> {
    return this._data;
  }

  @Input('data')
  set data(data: Observable<any>) {
    this._data = data;

    this.data.subscribe((dto) => {
      this.dataSource.data = dto;
      this.hasData = this.dataSource.data.length > 0;
      if (!this.hasAlreadyAnimated) {
        setTimeout(() => {
          this.hasAlreadyAnimated = true;
        }, 100 * dto.length + 200);
      }
    });
  }

  get columns(): TableColumn[] {
    return this._columns;
  }

  @Input('columns')
  set columns(columns: TableColumn[]) {
    this._columns = columns;

    this.hasFooter = !!this.columns.find((column) => !!column.footer);
    this.visibleColumnIdentifiers = this._columns.filter(column => column.visible).map(column => column.id);

    if (this.isVisibleActionColumn()) {
      this.visibleColumnIdentifiers.push('actions');
    }

    const options: AbstractControlOptions = {};
    const visibleColumns = this._columns.filter(column => !!column.visible).map((column) => {
      const validators = [];
      if (column.validators) {
        validators.push(column.validators);
      }
      validators.unshift('');
      options[column.id] = validators;

      return column;
    });
    this.formGroup = this.fb.group(options);

    visibleColumns.forEach((column) => {
      const validators: ValidatorFn[] = [];
      if (!!column.validators) {
        column.validators.forEach((validator) => validators.push(validator));
      }

      const formControl = new FormControl('', validators);
      this.formGroup.addControl(column.id, formControl);
    });

    // console.log('FORM GROUP', this.formGroup);
  }

  constructor(private fb: FormBuilder) {
  }

  public rightClickMenuRowIndex: TableRowIndex = -1;

  @ViewChild(MatSort, {static: true}) sort: MatSort;

  @Input()
  public settings: TableSettings = {
    filterable: false
  };

  @Output()
  public delete = new EventEmitter();

  @Output()
  public save = new EventEmitter();

  _data: Observable<any>;

  public hasFooter = false;

  _columns: TableColumn[];

  selection = new SelectionModel<any>(true, []);

  public isBatchMode = false;

  public formGroup: FormGroup;

  public visibleColumnIdentifiers: TableColumnIdentifier[];

  public dataSource: MatTableDataSource<EmployeeDTO>;

  public editRow: TableRowIndex = -1;
  public editMode = false;

  @Input()
  public rightClickMenu: RightClickMenu;

  @ViewChild(MatMenuTrigger)
  contextMenu: MatMenuTrigger;

  contextMenuPosition = { x: '0px', y: '0px' };

  public focusColumn: TableColumnIdentifier;

  /** Whether the number of selected elements matches the total number of rows. */
  public isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  public masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  public checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
  }

  public switchBatchMode() {
    this.isBatchMode = !this.isBatchMode;
    if (this.isBatchMode) {
      this.visibleColumnIdentifiers.unshift('select');
    } else {
      this.visibleColumnIdentifiers.shift();
    }
  }

  public isVisibleActionColumn(): boolean {
    return !!this.settings.deletable || !!this.settings.editable;
  }

  ngOnInit(): void {
    this.dataSource = new MatTableDataSource([]);
    this.dataSource.sort = this.sort;
  }

  public applyFilter(event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  public onEdit(row: TableRow) {
    console.log('ON EDIT', row);
    if (this.editMode) {
      console.log('TODO: Derzeit wird schon eine Zeile bearbeitet. Notification soll ausgegeben werden. Benutzer soll gefragt werden ob er wirklich die bisherige Eingabe verwerfen will.');
    }

    this.editRow = row.index;
    this.editMode = true;

    this.visibleColumnIdentifiers.filter((id: TableColumnIdentifier) => id !== 'actions').forEach((id: TableColumnIdentifier) => {
      this.formGroup.get(id).setValue(row.data[id]);
    })
  }

  public onCancelSave(event) {
    // console.log("ON CANCEL SAVE", event);
    this.editMode = false;
    this.editRow = -1;
  }

  public onSave(event) {
    console.log('SAVE EVENT', event);
    console.log('FORM VALUE', this.formGroup.value);
    this.save.emit({
      id: event.empId,
      data: this.formGroup.value
    });
    this.editMode = false;
    this.editRow = -1;
  }

  public isRowNotEdited(index: TableRowIndex) {
    return !this.editMode || this.editRow !== index;
  }

  onContextMenu(event: MouseEvent, cell: TableCell) {
    event.preventDefault();
    this.contextMenuPosition.x = event.clientX + 'px';
    this.contextMenuPosition.y = event.clientY + 'px';
    this.contextMenu.menuData = { cell };
    this.contextMenu.menu.focusFirstItem('mouse');
    this.contextMenu.openMenu();
    this.rightClickMenuRowIndex = cell.index;
    this.contextMenu.menuClosed.subscribe(() => {
      this.rightClickMenuRowIndex = -1;
      console.log('CLOSED RIGHT CLICK MENU');
    });
  }

  public onClickRow(row: TableRow) {
    if (this.settings.rowClick) {
      this.settings.rowClick(row);
    }
  }

  public onClickCell(cell: TableCell) {
    if (this.isBatchMode) {
      this.selection.toggle(cell.data);
    }

    const row: TableRow = {
      data: cell.data,
      index: cell.index
    };
    this.onClickRow(row);
  }

  public onDoubleClickRow(row: TableRow) {
    console.log('DOUBLE CLICK ROW');
    if ((!this.editMode || row.index !== this.editRow) && !!this.settings.editable) {
      this.onEdit(row);
    }
  }

  public onDoubleClickCell(cell: TableCell) {
    const row: TableRow = {
      data: cell.data,
      index: cell.index
    };
    this.focusColumn = cell.column.id;
    this.onDoubleClickRow(row);
  }

}
