import { Observable } from 'rxjs';
import { ValidatorFn } from '@angular/forms';

export type TableColumnName = string;
export type TableColumnIdentifier = string;
export type TableColumnProp = string;
export type TableRowIndex = number;
export type TableColumnInputType = string;
export type TableCellType = 'text' | 'checkbox';
export type TableRowCursor = 'normal' | 'pointer';

export interface TableColumn {
  id: TableColumnIdentifier;
  name: TableColumnName;
  prop: TableColumnProp;
  footer?: (data: any) => string;
  visible: boolean;
  validators?: ValidatorFn[];
  sortable?: boolean;
  input?: TableColumnTextInput | TableColumnAutocompleteInput;
  cellType?: TableCellType
}

export interface TableColumnInput {
  type: TableColumnInputType
}

export interface TableColumnTextInput {
  type: 'text'
}

export interface TableColumnAutocompleteInput extends TableColumnInput {
  type: 'autocomplete'
  options: string[]
}

// Can be removed?
export interface TableService<T> {
  findAll(): Observable<T[]>;
}

export interface TableSettings {
  filterable?: boolean,
  deletable?: boolean,
  editable?: boolean,
  allowBatchMode?: boolean,
  sortable?: boolean;
  rowClick?: (row: TableRow) => void;
  rowCursor?: TableRowCursor;
}

export interface TableRow {
  data: any; // TODO: DTO
  index: TableRowIndex
}

export interface TableCell extends TableRow {
  column: TableColumn
}

export interface RightClickMenuEntry {
  label: string,
  function: (cell: TableCell) => void
}

export type RightClickMenu = RightClickMenuEntry[];
