# Deployment

This directory contains stuff used on the deployment servers ([master](https://exampleapp.miguel-franken.com/) and [dev](https://exampleapp-dev.miguel-franken.com/)).
The GitLab CI pipeline executes the [deploy-server.sh](./deploy-server.sh) on the servers for each commit. This script then starts the database, backend and frontend. 
