#!/bin/bash
# Install Certbot
sudo add-apt-repository ppa:certbot/certbot
sudo apt install python-certbot-nginx

# Temporarly Deactivate Firewall
ufw disable

# Obtain keys
sudo certbot --nginx -d stubi.miguel-franken.com

# Create keystore.p12
cd /etc/letsencrypt/live/stubi.miguel-franken.com
openssl pkcs12 -export -in fullchain.pem -inkey privkey.pem -out keystore.p12 -name tomcat -CAfile chain.pem -caname root

# Transfer file from remote to local machine
rsync -a root@stubi.miguel-franken.com:/etc/letsencrypt/live/stubi.miguel-franken.com/keystore.p12 /Users/miguelfranken/iMac/git/stubi-backend/keystore.p12
