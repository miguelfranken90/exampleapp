#!/bin/bash
cd ../Backend
docker stop backend
docker rm backend
docker build -t miguelfranken/exampleapp-backend-dev .
docker push miguelfranken/exampleapp-backend-dev
