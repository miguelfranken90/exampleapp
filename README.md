# Example App

https://exampleapp.miguel-franken.com/ - https://exampleapp-dev.miguel-franken.com/

## Requirements
- [Java JDK](https://www.oracle.com/java/technologies/javase-jdk8-downloads.html) (v8)
- [Maven](https://maven.apache.org/) (>v3.6.1)
- [Node](https://nodejs.org/en/) (>v12.13.0)

## Getting Started
- `npm run start` in Frontend folder
- `mvn spring-boot:run` in Backend folder
- Log in with username `admin` and password `passwort`.

## Troubleshooting
### Getting Google Chrome to accept self-signed localhost certificate
`chrome://flags/#allow-insecure-localhost`

See https://stackoverflow.com/questions/7580508/getting-chrome-to-accept-self-signed-localhost-certificate for more information

## Documentation
- [Documentation Overview](https://miguelfranken90.gitlab.io/exampleapp)
- [Frontend Documentation (Dev Branch)](https://miguelfranken90.gitlab.io/exampleapp/frontend)
- [Backend Documentation (Dev Branch)](https://miguelfranken90.gitlab.io/exampleapp/backend)
- [Frontend Coverage Report (Dev Branch)](https://miguelfranken90.gitlab.io/exampleapp/coverage-frontend)
- [SonarCloud](https://sonarcloud.io/dashboard?id=miguelfranken90_exampleapp)
